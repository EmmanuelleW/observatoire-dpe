# 2021 04 21 2.0.0

* refacto des versions et changement de structure pour les contrôles de cohérences -> séparé en 3 objets , controle coherence audit, controle coherence DPE et svc controle coherence

# 2021 06 01 2.1.0 

* ajout d'un retour d'une propriete mode_test dans le json de retour DPE

* ajout d'une route traduction_xml_audit pour traduire les xml d'audit

# 47-faire-un-cas-test-traduit 

* la traduction a été implémentée dans utils pour être utilisé dans d'autres parties du code

# 2022-12-13 2.2.0

* ajout d'une route de traduction xml excel:  traduction_xml_to_excel_dpe (renvoi un excel à partir d'un xml dpe en entrée)

* ajout de routes de traduction xml avec suppression des enum_ : traduction_xml_audit_no_enum et traduction_xml_no_enum

# 2023-02-09 2.3.0

* conversion xml -> excel : correction d'un bug pour les éléments avec des données d'entrées mais sans données intermédiaires.

* ajout d'un convertisseur xml -> excel audit

* correction d'un bug qui faisait crasher l'export xml traduit , excel en cas de xsi:nil dans le xml source

# 2023-03-27 2.3.1

* fix sur la route /version pour la version du contrôle cohérence audit

# 2023-10-12 2.3.2

* fix Werkzeug==2.2.2 pour éviter des import error dans flask https://stackoverflow.com/questions/77213053/importerror-cannot-import-name-url-quote-from-werkzeug-urls