# 2021 12 05 1.1.0

* ajout d'un système de logging

* ajout d'une route health

* ajout d'une route version

* ajout de cas test valide pour dépot sur logement existant :
  
    * cas_test_immeuble_1_valid.xml
    
* requalification des cas tests valids pour le neuf et le tertiaire

# 2021 05 20 1.2.0

* ajout d'un warning spécifique pour gérer le cas particulier des coef_transparence_ets pour lesquels un vitrage majoritaire ne peut être déterminé.

* ajout des controles de cohérences des Umur,Upb,Uph,Uph0,Upb0

# 2021 05 27 1.3

* ajout documentation warning baie/mur -> cas des débords de façade. 

* passage de certains contrôles bloquants en warning logiciels sur les énergies pour prendre en compte le cas des énergies autres pour des usages != chauffage et ecs.

# 2021 07 02 1.4

* corrections de bugs sur le controle de cohérence administratif.

* ajout du controle de cohérence sur le tertiaire en v2

# 2021 07 16 1.5.0

* affichage des versions xsd et application

# 2021 07 23 1.5.3

* amélioration du catch d'erreur non prévues. 

# 2021 09 10 1.6.0

## améliorations application. 

* ajout de la version du moteur et du XSD dans les réponses.

* ajout d'une route controle_coherence_debug pour permettre de debugger la partie contrôle cohérence même avec des problèmes de validation xml en amont

## modification de controle de cohérences existants 

* gestion des parois non déperditives dans le contrôle de cohérence.  

* controle_coherence_existence_composants : ajout de warnings si installation chauffage et ecs sont vides. 

## ajout de controle de coherence etiquette

* V2 erreur bloquant si l'étiquette energie bilan ne correspond pas aux règles des doubles seuils de l'arrêté. 

* V2 erreur bloquant si l'étiquette GES ne correspond pas aux seuils sur les emissions GES.  

# 2021 10 08 1.7.0 

## ajout de contrôle de cohérence vérifiant les dates de validité des versions du DPE.

* un contrôle de cohérence est ajouté pour vérifier la version des DPE et les rejeter s'ils correspondent à une version 
qui n'est plus en vigueur. 

# 2021 10 14 1.8.0

## ajout d'un contrôle de cohérence qui refuse les dépôt de DPE avec des dates d'etablissement supérieure à la date du jour

* pour palier à des dérives constatées, un contrôle de cohérence est mis en place pour refuser le dépôt de DPE avec des dates d'établissement > date du jour. 

# 2021 10 18 1.9.0

* correction d'un bug qui limitait le controle de cohérence sur les tables de valeurs.

* ajout d'une fonctionalité pour desactiver les contrôles de date pour debug (variable environement)

# 2021 11 16 1.10.0 Contrôles bloquants passés en warning uniquement pour la version 2

## ajout d'une restriction de date butoire à partir desquels des DPE "en retard" sont refusés

* pour chaque version une date butoire est mise en place à partir de laquelle plus aucun DPE de cette version n'est acceptée y compris
si la date d'établissement du DPE précède cette date butoir (à partir de cette date plus de publication "en retard" de DPE établis avec des anciennes versions ne sont autorisés)

## ajout controle de cohérence générateur cascade

* vérifie que la déclaration de valeur 0 sur rendement_generation,conso_ch se fait bien uniquement pour les générateurs combustion cascade ou un appoint de base+ appoint collectif. 

## contrôles de cohérences bloquants passés en warning

de manière temporaire les contrôles de cohérences bloquants sont passés en warning dans la version 2 du modèle de données. 
Cette adaptation est réalisée pour permettre aux éditeurs de progressivement corriger ces problèmes sans pour autant bloquer le processus de dépôt de DPE
pendant une durée déterminée. 
Les versions ultérieures du modèle rétabliront ces contrôles de cohérences comme bloquants. 
Certains contrôles qui étaient opérés en v1 (contrôle des dates, contrôles de la structure du xml ) sont conservés en erreurs. Seuls les contrôles de cohérence métier sont 
basculés en avertissement.

ainsi les messages des contrôles de cohérences basculés en warnings sont transférés comme suit

erreur_saisie -> warning_saisie

erreur_logiciel -> warning_logiciel

le type de message est basculé de "bloquant" à "bloquant_avertissement"

un texte en préambule précise pour chaque message que ce problème sera basculé en erreur dans la prochaine version

Exemple :

```

CET AVERTISSEMENT SERA CONSIDERE COMME UNE ERREUR DANS LA PROCHAINE VERSION:
 
mauvaise correspondance entre la valeur tv_uw_id:343
avec les données connexes suivantes :
{'enum_type_baie_id': [4], 'type_baie': 'Fenêtres battantes', 'enum_type_materiaux_menuiserie_id': [5], 'type_materiaux_menuiserie': 'PVC', 'ug': 2.4, 'uw': 2.4}
et l'énumérateur enum_type_materiaux_menuiserie_id:{3: 'Bois'}.
La valeur attendue de l'énumérateur enum_type_materiaux_menuiserie_id doit être une des suivantes:
{5: 'PVC'}

```


# 2021 11 23 1.10.1 controle coherence étiquette bloquant

* passage de contrôle de cohérence etiquette en bloquant

* ajout de la listes des descriptions des objets concernés dans le message en cas de warning ou d'erreur. 


# 2021 12 03 1.10.2 contrôle cohérence d'énergie en warning bloquant.  

* passage du contrôle de cohérence energie en warning bloquant 

* ajout d'une vérification que l'electricité est déclarée dans sortie_par_energie

* fix sur le contrôle de cohérence sur enum_periode_construction_id et les tv_umur_id,tv_uph_id,tv_upb_id dans le cas d'un enum_periode_isolation_id déclaré.
la période d'isolation écrase bien la période de construction lorsque celle ci est déclarée. 

# 2021 12 10 1.10.3 fix d'un bug sur controle de cohérence tv_value_simple 

* fix bug quand u0 n'est pas déclaré -> remplacement par un message explicite controle de cohérence

# 2021 12 14 1.11.0 ajout d'un contrôle de cohérence sur les consommation 0 ECS et chauffage

* pour gérer le cas particulier où fecs et fch peuvent être égaux à 1. Un contrôle de cohérence vérifie que les consommations = 0 sont utilisés uniquement dans ce cas

* ajout du contrôle de cohérence système (oubli d'activation) : contrôle de cohérence qui ne génère que des warnings

* ajout du contrôle de cohérence pont thermique (oubli d'activation) : contrôle de cohérence qui ne génère que des warnings

* ajout d'une possibilité de passer des DPE en version antérieure dans le cas d'une réédition. 

# 2022 01 06 1.12.0 corrections diverses
 
* desactivation d'une partie du contrôle de cohérence pont thermique/menuiserie en attendant le rajout de enum_type_pose_id pour les portes. 

* correction du contrôle de cohérence energie pour ne pas bloquer un dépôt lorsqu'un générateur consomme 0 et que l'energie n'est pas déclarée en sortie. 

* ajout d'un bypass pour ne pas contrôler la correspondance tv vs valeur dans le cas d'une double fenêtre pour sw

* fix sur le contrôle de cohérence sur la table SEER qui n'était pas proprement déclenchée.  

* bugfix : le nom du composant dans le cas d'une vérification de cohérence d'isolation pouvait ne pas être le bon ceci a été corrigé

# 2022 01 21 1.13.0
 
* fix controle de cohérence pont thermique avec ajout des portes dans le contrôles des ponts thermiques menuiseries/mur. 

* AJOUT : contrôle de cohérence qui vérifie que les cle_repartition_ch, cle_repartition_ecs,cle_repartition_clim et cle_repartition_ventilation sont bien déclarées dans le cas d'un DPE appartement à partir de l'immeuble

# 2022 03 02 1.14.0

* controle_coherence tv_value correction sur le contrôle sur le seer

# 2022 04 15 1.15.0

* controle_coherence_correspondance_saisi_value : désactivation du contrôle de cohérence lorsque l'on est dans le cas d'une double fenêtre. 

* désactivation du warning période isolation pour les composants qui ne sont pas murs,plancher bas ou plancher haut

* ajout d'une gestion des paroi dont l'isolation est déjà prise en compte dans la paroi nue pour ne pas déclencher le warning du controle_coherence_enveloppe
sur la cohérence d'isolation entre la déclaration d'isolation et le calcul du b. 

# 2021 04 21 Audit 1.0.0

* initialisation d'un contrôle cohérence audit qui est une simple validation xml

# 2022 06 02 DPE-1.16.0 Audit 0.1.0

* ajout d'un contrôle de cohérence qui vérfie que dpe_immeuble_associe est bien déclaré pour un dpe appartement à partir de l'immeuble

* ajout d'un contrôle de cohérence d'unicité de reference

# 2022-09-28 DPE-1.17.0 Audit-1.0.0

## controle coherence dpe

* desactivation du warning appel BAN suggestif dans le controle de cohérence administratif

## controle coherence audit

* Audit reg : Modification du contrôle de cohérence controle_coherence_unicite_reference(), pour être compatible avec les logements de l'audit règlementaire (etape_travaux)

* rapport d'erreur : ajout des références des objets concernés en plus des descriptions dans les messages

* rapport d'erreur audit : ajout d'etape travaux et scénario travaux dans les messages de warning pour retrouver plus facilement les éléments qui posent problème

## refactoring classe mutualisée

* feat(controle_coherence) : fusion des engine controle coherence audit et DPE et partage de la plupart des méthodes dans CoreEngine 

# 2023-02-14 DPE-1.18.0 Audit-1.1.0

## controle coherence commun DPE/AUDIT 

* ajout d'un contrôle de cohérence entre la méthode d'application (enum_methode_application_dpe_log_id) et le modèle de DPE (enum_modele_dpe_id)

* ajout de la condition besoin_ch = 0 pour autoriser une saisie conso_ch = 0 au niveau des générateurs

* ajout d'un contrôle de cohérence si double_fenetre = 1 alors vérification que la sous structure de double fenetre est déclarée

* ajout d'un contrôle de cohérence qui vérifie que pveilleuse est déclarée pour les systèmes à combustion avec veilleuse lorsque ceux cis sont saisis par défaut. 

* ajout d'un contrôle de cohérence réseau de chaleur :

  * avertissement si un identifiant réseau n'existe pas dans la table de l'arrêté. Si un identifiant réseau est saisi mais qu'il n'est pas documenté dans l'arrêté cela envoi un avertissement

  * contrôle bloquant : si la date d'arrêté de réseau de chaleur est non déclarée ou expirée (remplacée par un arrêté plus récent), le DPE est refusé pour dépôt.

* ajout d'un controle de cohérence sur le calcul de ue:

  * vérifie que calcul_ue = 1 pour les 3 adjacences pour lesquelles le calcul de UE doit être effectué et que calcul_ue = 0 pour les autres

  * vérifie que surface_ue,ue,perimetre_ue sont bien déclarés pour les 3 adjacences pour lesquelles le calcul de UE doit être effectué

  * vérifie que surface_ue,ue,perimetre_ue ne sont pas déclarés pour les autres adjacences

  * vérifie que ue = upb_final pour les 3 adjacences pour lesquelles le calcul de UE doit être effectué 

  * vérifie que upb = upb_final pour les autres adjacences 

* ajout d'un contrôle de cohérence qui vérifie qu'au moins un logement_visite est déclaré dans dpe_immeuble.logement_visite_collection

# 2023-03-21 DPE-1.18.0 Audit-1.1.1

* fix: correction d'un bug du contrôle de cohérence pour l'audit 1.0 qui entrainait des contrôles bloquants indésirés 

# 2023-03-27 DPE-1.18.2 Audit-1.1.2

* le contrôle de cohérence sur l'arrêté de réseau de chaleur prend en compte le nouvel arrêté du 16 mars 2023

# 2023-04-26 DPE-1.18.2 Audit-1.2.0 V2.0 Audit règlementaire

## contrôle coherence commun

* (fix) : contrôle de cohérence pveilleuse ne se déclenchait pas proprement, il est réintroduit sous forme d'avertissement non bloquant pour ne pas bloquer de manière indésirable le dépôt de DPE ou d'audit

## contrôles coherence DPE

* (fix) : contrôle de cohérence rsee. suppression du contrôle sur les balises RSENV (la partie RSENV n'est pas obligatoire pour un RSEE en phase DP)

## Contrôles coherences audit

**Contrôles Bloquants en v2.0 :**
	
1. Technique : 

* 	Contrôler l'unicité id d'étape par scénario - controle_coherence_unicite_etape_par_scenario
*   Contrôler que tous les logements, SAUF logement de type « état initial », possèdent « etape_travaux » - controle_coherence_presence_etape_travaux
* 	Contrôler que les références dans « travaux/reference_collection » existent bien dans le « logement » - controle_coherence_reference_travaux_existent
* 	Contrôler que les consommations 5 usages, les emissions de CO2 5 usages et les classes dans « etape_travaux » correspondent bien à celles dans « sortie » issues du calcul 3CL (tolérance à l’unité). - controle_coherence_etape_travaux_sortie_dpe
* 	Contrôler que pour le logement « état initial » que tous les « enum_etat_composant_id » soit à "1"="initial" - controle_coherence_etat_composant
* 	Contrôler que pour toutes les étapes de travaux (qui ne sont pas « état initial »), au moins 1 objet ait un « enum_etat_composant_id » à "2"="neuf ou rénové" - controle_coherence_etat_composant
* 	Contrôler que pour toutes les étapes de travaux, le « cout » de l’étape corresponde à la somme des coûts dans « travaux_collection » et « travaux_induits_collection » (tolérance à 100€ près) - controle_coherence_etape_travaux_cout



2. Métiers : 
*  Existence d'un seul logement de type « état initial » - controle_coherence_presence_etat_initial
*  Le « scénario en une étape "principal" » existe ET ne contient qu’une seule étape de travaux correspondant à l’ « étape finale » - controle_coherence_scenario_mono_etape
*  Le « scénario multi étapes "principal" » existe ET contient au moins 2 étapes de travaux dont une « étape première » et une « étape finale »  - controle_coherence_scenario_multi_etapes
*  Pour ces deux scénarios, l’ « étape finale » doit atteindre l’étiquette B. Si dérogation, alors il faut un saut de deux classes entre l’ « état initial » (avant travaux) et l’étape finale. - controle_coherence_etape_finale
*  Pour le « scénario multi étapes "principal" », il faut que l’ « étape première » permette de réaliser un gain d'au moins une classe et au minimum d'atteindre la classe E - controle_coherence_etape_premiere
*  Vérifier pour chaque logement, que la méthode DPE (dans :"enum_methode_application_dpe_log_id") soit valide pour l'audit règlementaire. - controle_coherence_modele_methode_application
*  Vérifier que tous les logements ont une méthode DPE cohérents (dans :"enum_methode_application_dpe_log_id"), c'est à dire un même type de bâtiment (maison, appartement, immeuble) - controle_coherence_type_batiment_constant
*  Vérifier que lors que la méthode DPE immeuble est utilisée dans l'état initial (dans :"enum_methode_application_dpe_log_id") que les logements visités doivent être déclarés dans le xml - controle_coherence_logement_visite (Reprise du controle de cohérence DPE)
*  Vérifier l'audit ne contienne pas à la fois un numéro audit à remplacer et un numéro audit à mettre à jour - controle_coherence_choix_maj_ou_remplacer

**Contrôles non bloquants - Warnings :** 

* Pour les bâtiments de classe de performance F ou G avant travaux, le « scénario multi étapes "principal" » comporte une « étape intermédiaire » permettant d'atteindre au moins la classe C - controle_coherence_scenario_multi_etapes_passoire
* Lorsqu'il y a plus de 3 étapes dans un scénario de travaux - controle_coherence_seuil_3_etapes
* Lorsque l'auditeur n'a renseigné aucun encadré destiné aux observations (recommandation) - controle_coherence_presence_recommandation
* Lorsque la date d'établissement de l'audit est antérieure, ou même jour, par rapport à la date de visite - controle_coherence_date_visite_date_etablissement
* Pour les dérogations, vérifier que les six postes de travaux de rénovation énergétique ont été traités pour les deux scénarios : isolation des murs, l'isolation des planchers bas, l'isolation de la toiture, le remplacement des menuiseries extérieures, la ventilation, la production de chauffage et d'eau chaude sanitaire (via « enum_lot_travaux_audit_id ») - controle_coherence_six_postes_travaux
* Lorsque des travaux, dans "travaux_collection", ont des coûts nuls - controle_coherence_cout_nul
* Contrôler l'ordre de grandeur (facteur 10) des consommations dans etape_travaux par rapport aux sorties DPE - objectif, signaler un oubli de division par la surface habitable - controle_coherence_conso_etape_travaux
* Pour les scénarios complémentaires, l’ « étape finale » doit atteindre l’étiquette B. Si dérogation, alors il faut un saut de deux classes entre l’ « état initial » (avant travaux) et l’étape finale. - controle_coherence_etape_finale


# 2023-05-16 DPE-1.18.2 Audit-1.3.0 V2.0 Audit règlementaire

## Contrôles coherences audit

* (fix) Suppression de "controle_coherence_choix_maj_ou_remplacer()"


# 2023-06-09 DPE-1.18.2 Audit-1.4.0 V2.0 Audit règlementaire

## Contrôles coherences audit

* Modification du comportement du controle de cohérence audit: 
  * Les controles de cohérence spécifiques à l'audit ne sont lancés que si les controles de cohrences de l'application de la méthode de calcul 3CL (methode DPE) n'ont pas remonté d'erreurs bloquantes.

* (fix) correction des controles pour éviter un crash du moteur dans le cas d'un XML sans aucun etape_travaux:
  * controle_coherence_etape_premiere
  * controle_coherence_six_postes_travaux
  * controle_coherence_etape_finale
  * controle_coherence_scenario_mono_etape
  * controle_coherence_scenario_multi_etapes
  * controle_coherence_logement_visite
  * controle_coherence_presence_etat_initial
  * controle_coherence_presence_etape_travaux
  * controle_coherence_scenario_multi_etapes_passoire
  * controle_coherence_type_batiment_constant


# 2023-06-09 DPE-1.18.2 Audit-1.4.1 V2.0 Audit règlementaire

## Contrôles coherences audit

* (fix) correction du controle de cohérence audit : controle_coherence_presence_recommandation() pour gérer le cas des recommandations à None.


# 2023-06-12 DPE-1.18.3 Audit-1.4.2 V2.0 Audit règlementaire

## Contrôles coherences DPE et Audit

* (fix) correction d'un bug qui écrasait les avertissement logiciels lorsque des avertissements qui vont devenir bloquant dans une future version sont présents

## Contrôles coherences audit

* (fix) controle_coherence_etape_premiere() et controle_coherence_etape_finale() ajout d'une condition 'class_etat_initial is not None'
* (fix) controle_coherence_scenario_multi_etapes_passoire() utilisation de la classe dpe issue de l'élément 'sortie' au lieu de l'élément 'etape_travaux' (qui peut être nulle)


# 2023-06-12 DPE-1.18.3 Audit-1.4.3 V2.0 Audit règlementaire

## Contrôles coherences audit

* (fix) controle_coherence_etape_travaux_cout() pour que le controle puisse fonctionner normalement indépendamment du positionnement de la balise 'coût' dans 'etape_travaux'

# 2023-06-20 DPE-1.18.3 Audit-1.4.4 fix controle audit

## Contrôles coherences audit

* (fix) fix d'un bug du moteur de contrôle de cohérence pour gérer un cas spécifique où aucun état initial n'était déclaré et une dérogation existe. 

# 2023-08-29 DPE-1.18.4 Audit-1.4.5 fix message d'erreur version antérieures

## Contrôles coherences dpe/audit

* (fix) fix d'un bug sur le message de retour quand une version n'est plus valide. La liste des versions valides est maintenant corrigée

# 2023-09-04 DPE-1.18.4 Audit-1.4.6 fix d'un bug de non déclenchement de l'erreur de version obsolète pour les audit

## Contrôles coherences audit

* (fix) fix d'un bug de non déclenchement de l'erreur de version obsolète pour les audit avec des dates d'établissement inférieures au 01-09-2023

# 2023-10-12 DPE-1.18.4 Audit-1.4.7 ajout contrôle coherence sur la présence d'un numéro dpe

## Contrôles coherences audit

* AJOUT du controle_coherence_presence_numero_dpe, qui contrôle la présence d'un numéro DPE, lors que le contexte de l'audit est règlementaire (contrôle non bloquant)


# 2023-10-25 DPE-1.18.4 Audit-1.5.0 ajout contrôle coherence pour l'audit 2.1 (ubat_base et ventilation)

## Contrôles coherences audit

* BLOQUANT (en 2.1):
  - AJOUT du controle_coherence_presence_derogation_ventilation : si la dérogration ventilation est utilisée pour l'étape (objet logement), alors une dérogation doit être présente dans enum_derogation_ventilation_id
  - AJOUT du controle_coherence_abscence_derogation_ventilation : si aucune étape (objet logement) de l'audit n'utilise la dérogration ventilation, alors la balise enum_derogation_ventilation_id doit être à "abscence de dérogation"

* Warning (non bloquant):
  - AJOUT du controle_coherence_ubat_base_ubat : vérifie que pour les scénarios mono et multi étapes principaux, que le Ubat de l'étape finale soit inférieur au Ubat_base (condition BCC réno)
  - AJOUT du controle_coherence_etat_ventilation : vérifie que pour les étapes de travaux ont un état de ventilation fonctionnelle (condition BCC réno)

* Retire le controle_coherence_scenario_multi_etapes_passoire (non bloquant), qui contrôlait que, pour les batiments passoires, le « scénario multi étapes "principal" » comporte une « étape intermédiaire » permettant d'atteindre au moins la classe C


# 2023-12-08 DPE-1.18.4 Audit-1.5.1 fix du controle_coherence_presence_derogation_ventilation pour l'audit 2.1

## Contrôles coherences audit

* (fix) fix du controle_coherence_abscence_derogation_ventilation concernant l'affichage des objets concernés par le controle.