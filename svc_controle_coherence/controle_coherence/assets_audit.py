import numpy as np
from datetime import datetime


# configuration des versions de l'audit, incluant le nom du fichier XSD à utiliser ainsi que ses dates de validité

# les versions anterieurs documentées ici appliquent les blocker as warning comme des warnings
AUDIT_VERSION_ANTERIEUR = ['2.0']
# AUDIT_VERSION_ANTERIEUR = ['1.0', '0.1']

versions_audit_cfg = {
    '0.1': {
        'xsd_file': 'audit_regv1.xsd',
        'start_date': '2000-01-01',
        'end_date': '2023-09-01',
        'end_date_compare_now': '2023-09-01',
        'is_future': False,

    },
    '1.0': {
        'xsd_file': 'audit_regv1.xsd',
        'start_date': '2000-01-01',
        'end_date': '2023-09-01',
        'end_date_compare_now': '2023-09-01',
        'is_future': False,

    },
    '1.1': {
        'xsd_file': 'audit_regv1.1.xsd',
        'start_date': '2000-01-01',
        'end_date': '2023-09-01',
        'end_date_compare_now': '2023-09-01',
        'is_future': False,

    },
    '2.0': {
        'xsd_file': 'audit_v2.0.xsd',
        'start_date': '2000-01-01',
        'end_date': '2024-02-01',
        'end_date_compare_now': '2024-07-31',
        'is_future': False,

    },
    '2.1': {
        'xsd_file': 'audit.xsd',
        'start_date': '2023-10-01',
        'end_date': '2200-01-01',
        'end_date_compare_now': '2200-01-01',
        'is_future': True,

    },

}

def get_current_valid_versions_audit():

    # liste des versions valides
    now = datetime.now()
    current_valid_versions = list()
    for k, cfg in versions_audit_cfg.items():
        start_date = datetime.fromisoformat(cfg['start_date'])
        end_date = datetime.fromisoformat(cfg['end_date'])
        end_date_compare_now_version = datetime.fromisoformat(cfg['end_date_compare_now'])

        if (start_date <= now) & (end_date >= now) & (end_date_compare_now_version >= now):
            current_valid_versions.append(k)
    return current_valid_versions