import pytest
import re
import sys
import controle_coherence
from controle_coherence.assets_audit import versions_audit_cfg
from controle_coherence.utils import convert_xml_text, remove_sub_el, create_sub_el
from controle_coherence.controle_coherence import EngineDPE, EngineAudit
from controle_coherence.utils import reload_package, convert_xml_text, element_to_value_dict, set_xml_values_from_dict,_set_version_audit_to_valid_dates,_set_version_dpe_to_valid_dates
from controle_coherence.assets_dpe import expected_components, versions_dpe_cfg
from lxml import etree
import copy
from controle_coherence.controle_coherence import ReportDPE, ReportAudit
from datetime import timedelta, datetime
import os

VALID_CASES_DPE = ['cas_test_appt_2_neuf_light_valid.xml', 'cas_test_appt_2_neuf_valid.xml',
                   'cas_test_immeuble_1_valid.xml',
                   'cas_test_maison_1_valid.xml',
                   "cas_test_appartement_1_valid.xml",
                   'cas_test_immeuble_1_valid_double_fenetre.xml',
                   # 'cas_test_immeuble_1_valid_v2.2.xml',
                   # 'cas_anomalie_veilleusev2.2.xml',
                   # "cas_test_immeuble_1_valid_reseau_chaleurv2.2.xml",
                   'cas_test_immeuble_1_valid_tout_chaudiere.xml',
                   'cas_test_immeuble_1_valid_avec_reference.xml', 'cas_test_immeuble_1_valid_ecs_solaire_expert.xml', 'cas_test_tertiaire_1_vierge_valid.xml',
                   'cas_test_immeuble_1_valid_reseau_chaleur.xml',
                   'cas_test_immeuble_1_valid_generation_cascade.xml',
                   'cas_test_immeuble_1_valid_ss_masque_solaire.xml', 'cas_test_tertiaire_1_neuf_valid.xml',
                   'cas_test_appartement_a_partir_immeuble_valid.xml']


VALID_CASES_AUDIT = ['cas_test_audit_maison_1_v2.0_valid.xml',
                     'cas_test_audit_immeuble_v2.0_valid.xml',
                     'cas_test_audit_immeuble_v2.1_valid.xml',
                     'cas_test_audit_maison_1_v2.1_valid.xml',
                     'DPE_Audit_EDL_v2.1_valid.xml']

CRASH_CASES_EDL_AUDIT = ['23-04-814_envoie-ademe-AUDIT.XML','DPE1.1.xml','DPE2.2.xml','DPE_Audit.xml','DPE_Audit 1.xml','DPE.xml','V1.0-AUDIT.XML', 'DPE_Audit - error derogation ventil.xml']

CRASH_CASES_AUDIT = ['cas_test_audit_maison_1_v2.0_super_invalid.xml','DPE_Audit_max_None_no_etape_travaux.xml','DPE_Audit_max_None.xml']

CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST = int(os.getenv('CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST', 0))

print('CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST', CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST)


# TODO : _make_all_version_valid
# TODO : test date invalid. 




def test_valid_dates_and_versions():
    # A UPDATE à chaque patch
    engine = EngineDPE()
    assert((set(['1.0','1.1','2',"2.1","2.2"])&set(engine.current_valid_versions))==set())
    engine = EngineAudit()
    assert((set([])&set(engine.current_valid_versions))==set())


def test_date_valid():
    for k, v in versions_dpe_cfg.items():
        datetime.fromisoformat(v['start_date'])
        datetime.fromisoformat(v['end_date'])
        datetime.fromisoformat(v['end_date_compare_now'])
        if 'end_date_edit' in v:
            datetime.fromisoformat(v['end_date_edit'])

@pytest.mark.parametrize("valid_example", VALID_CASES_DPE)
def test_run_controle_coherence_dpe(valid_example):
    try:
        _set_version_dpe_to_valid_dates()
        engine = EngineDPE()
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
        dpe = etree.parse(f, parser)
        engine.run_controle_coherence(dpe)
        # f = str((engine.mdd_path / 'exemples_defaut' / 'DPE_logement.xml'))
        # dpe = etree.parse(f, parser)
        engine.run_controle_coherence(dpe)
        el = dpe.find('*//nom_proprietaire_installation_commune')
        el.getparent().remove(el)
        dpe = etree.parse(f, parser)
        engine.run_controle_coherence(dpe)

        # vérification v2
        f = str((engine.mdd_path / 'exemples_metier' / valid_example))
        dpe = etree.parse(f, parser)
        report = engine.run_controle_coherence(dpe)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

        # test rétrocompatibilité 2.2
        f = str((engine.mdd_path / 'exemples_metier' / valid_example))
        dpe = etree.parse(f, parser)
        dpe.find('*//enum_version_id').text='2.2'
        report = engine.run_controle_coherence(dpe)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

        # test retrocompatibilité numero_fiscal_local

        f = str((engine.mdd_path / 'exemples_metier' / valid_example))
        dpe = etree.parse(f, parser)
        if dpe.find('*//numero_fiscal_local') is not None:
            dpe.find('*//numero_fiscal_local').text = '2A01234567'
        report = engine.run_controle_coherence(dpe)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

        # # vérification rétrocompatibilité v1 : ON NE GERE PLUS LA RETROCOMPATIBILITE V1
        # dpe = etree.parse(f, parser)
        # dpe.find('*//enum_version_id').text = '1'
        # report=engine.run_controle_coherence(dpe)
        # nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        # assert (report['validation_xsd']['valid'] == True)
        # assert (nb_errors == 0)
    except AssertionError as e:
        if len(report['validation_xsd']['error_log'])>0:

            print(report['validation_xsd']['error_log'][0])
        print(f'nombre d erreurs : {nb_errors}')
        for err in report['erreur_logiciel'] + report['erreur_saisie']:
            print("=================== ERROR =========================")
            print(err['thematique'])
            print(err['message'])
            print(err['objets_concerne'])
        print(f)
        raise e




def test_controle_coherence_date():
    _set_version_dpe_to_valid_dates()
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1_v1.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    print(report['erreur_logiciel'])
    print(versions_dpe_cfg)
    assert (len(report['erreur_logiciel']) == 0)

    for k, v in versions_dpe_cfg.items():
        v['start_date'] = '2199-01-01'
        v['end_date'] = '2200-01-01'

    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_logiciel']) == 1)

    for k, v in versions_dpe_cfg.items():
        v['start_date'] = '1999-01-01'
        v['end_date'] = '2000-01-01'

    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_logiciel']) == 1)

    for k, v in versions_dpe_cfg.items():
        v['start_date'] = '1999-01-01'
        v['end_date'] = '2021-07-30'
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)

    assert (len(report['erreur_logiciel']) == 0)

    for k, v in versions_dpe_cfg.items():
        v['start_date'] = str(datetime.date(datetime.now() - timedelta(days=1)))
        v['end_date'] = '2200-01-01'
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_logiciel']) == 0)

    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_logiciel']) == 0)

    for k, v in versions_dpe_cfg.items():
        v['start_date'] = str(datetime.date(datetime.now() - timedelta(days=1)))
        v['end_date'] = '2200-01-01'
        v['end_date_compare_now'] = str(datetime.date(datetime.now() - timedelta(days=1)))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_logiciel']) == 1)

    future = (datetime.now() + timedelta(1)).date().strftime('%Y-%m-%d')
    dpe.find('*//date_etablissement_dpe').text = future
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    assert (len(report['erreur_saisie']) == 1)

def test_controle_coherence_dates_audit():
    _set_version_audit_to_valid_dates()
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_immeuble_v2.0_valid.xml'))
    audit = etree.parse(f, parser)
    audit.find('*//enum_version_audit_id').text = "1.1"
    report = ReportAudit()
    engine.validation_version_and_dates(audit, report)
    report = report.generate_report(audit, engine)
    print(report['erreur_logiciel'])
    print(versions_audit_cfg)
    assert (len(report['erreur_logiciel']) == 0)

    for k, v in versions_audit_cfg.items():
        v['start_date'] = '2199-01-01'
        v['end_date'] = '2200-01-01'

    audit = etree.parse(f, parser)
    audit.find('*//enum_version_audit_id').text = "1.1"
    report = ReportAudit()
    engine.validation_version_and_dates(audit, report)
    report = report.generate_report(audit, engine)
    print(report['erreur_logiciel'])
    print(versions_audit_cfg)
    assert (len(report['erreur_logiciel']) == 1)

    _set_version_audit_to_valid_dates()

    for k, v in versions_audit_cfg.items():
        v['start_date'] = '2000-01-01'
        v['end_date'] = '2200-03-01'
        v['end_date_compare_now'] = '2000-03-01'

    audit = etree.parse(f, parser)
    audit.find('*//enum_version_audit_id').text = "1.1"
    report = ReportAudit()
    engine.validation_version_and_dates(audit, report)
    report = report.generate_report(audit, engine)
    print(report['erreur_logiciel'])
    print(versions_audit_cfg)
    assert (len(report['erreur_logiciel']) == 1)


    for k, v in versions_audit_cfg.items():
        v['start_date'] = '2000-01-01'
        v['end_date'] = '2000-03-01'
        v['end_date_compare_now'] = '2200-03-01'

    audit = etree.parse(f, parser)
    audit.find('*//enum_version_audit_id').text = "1.1"
    report = ReportAudit()
    engine.validation_version_and_dates(audit, report)
    report = report.generate_report(audit, engine)
    print(report['erreur_logiciel'])
    print(versions_audit_cfg)
    assert (len(report['erreur_logiciel']) == 1)


def test_controle_coherence_date_edit():
    _set_version_dpe_to_valid_dates()
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    # cas sans dpe_a_remplacer
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1.1"

    # set all dates to invalid
    for k, v in versions_dpe_cfg.items():
        v['start_date'] = str(datetime.date(datetime.now() - timedelta(days=1)))
        v['end_date'] = str(datetime.date(datetime.now() - timedelta(days=1)))
        v['end_date_compare_now'] = str(datetime.date(datetime.now() - timedelta(days=1)))
        if 'end_date_edit' in v:
            v['end_date_edit'] = str(datetime.date(datetime.now() - timedelta(days=1)))
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    # invalid as it should be
    assert (len(report['erreur_logiciel']) == 1)

    # set dates edit to valid
    for k, v in versions_dpe_cfg.items():

        if 'end_date_edit' in v:
            v['end_date_edit'] = str(datetime.date(datetime.now() + timedelta(days=1)))

    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    # still not valid because no dpe_a_remplacer
    assert (len(report['erreur_logiciel']) == 1)

    # cas avec dpe_a_remplacer
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = "1.1"
    report = ReportDPE()
    engine.validation_version_and_dates(dpe, report)
    report = report.generate_report(dpe, engine)
    # should be valid
    assert (len(report['erreur_logiciel']) == 0)


def test_blocker_as_warning_for_v2():
    _set_version_dpe_to_valid_dates()

    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    dpe.find('*//enum_version_id').text = '2.2'
    report = engine.run_controle_coherence(dpe)

    nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
    nb_warnings = len(report['warning_saisie']) + len(report['warning_logiciel'])
    assert (nb_errors > 0)
    nb_tot = nb_warnings + nb_errors

    # dpe.find('*//enum_version_id').text = '2'
    # report = engine.run_controle_coherence(dpe)
    #
    # nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
    # nb_warnings = len(report['warning_saisie']) + len(report['warning_logiciel'])
    #
    # assert (nb_errors == 2)  # controle coherence etiquette
    # assert (nb_tot == (nb_errors + nb_warnings))


def test_controle_coherence_adminsitratif():
    _set_version_dpe_to_valid_dates()

    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    engine.controle_coherence_administratif(dpe, report)

    assert (len(report.error_software) == 0)
    assert (len(report.warning_input) == 0)

    dpe.find('administratif//enum_statut_geocodage_ban_id').text = '2'
    compl_etage_appartement = dpe.find('*//adresse_bien//compl_etage_appartement')
    compl_etage_appartement.getparent().remove(compl_etage_appartement)
    dpe.find('administratif//ban_score').text = '0.5'
    dpe.find('administratif//ban_type').text = '0.5'
    ban_city = dpe.find('*//adresse_bien//ban_city')
    ban_city.getparent().remove(ban_city)
    ban_housenumber = dpe.find('*//adresse_bien//ban_housenumber')
    ban_housenumber.getparent().remove(ban_housenumber)
    engine.controle_coherence_administratif(dpe, report)
    assert (len(report.error_software) == 2)
    assert (len(report.warning_input) == 4)
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    dpe.find('administratif//ban_type').text = 'municipality'
    engine.controle_coherence_administratif(dpe, report)
    assert (len(report.warning_input) == 1)

    report = ReportDPE()
    dpe = etree.parse(f, parser)
    adresse_proprietaire_commun = dpe.find('*//adresse_proprietaire_installation_commune')
    adresse_proprietaire_commun.getparent().remove(adresse_proprietaire_commun)
    dpe.find('administratif//ban_type').text = 'municipality'
    engine.controle_coherence_administratif(dpe, report)
    assert (len(report.warning_input) == 1)


def test_controle_coherence_dpe_tertiaire():
    _set_version_dpe_to_valid_dates()

    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_tertiaire_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = engine.run_controle_coherence(dpe)
    assert (len(report['erreur_logiciel']) == 0)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_tertiaire_1_vierge_valid.xml'))
    dpe = etree.parse(f, parser)
    report = engine.run_controle_coherence(dpe)
    assert (len(report['erreur_logiciel']) == 0)

    dpe.find('*//enum_methode_application_dpe_ter_id').text = '1'
    report = engine.run_controle_coherence(dpe)
    print(report['erreur_logiciel'])

    assert (len(report['erreur_logiciel']) == 2)


def test_controle_coherence_etiquette_dpe():
    engine = EngineDPE()
    report = ReportDPE()
    self = engine
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//ep_conso/ep_conso_5_usages_m2').text = '421'
    dpe.find('*//ep_conso/classe_bilan_dpe').text = 'G'
    dpe.find('*//classe_emission_ges').text = "A"
    dpe.find('*//emission_ges_5_usages_m2').text = "5"

    engine.controle_coherence_etiquette(dpe.find('logement'), report)

    assert (len(report.error_software) == 0)

    dpe.find('*//enum_zone_climatique_id').text = "7"
    engine.controle_coherence_etiquette(dpe.find('logement'), report)

    assert (len(report.error_software) == 0)
    dpe.find('*//enum_classe_altitude_id').text = "3"
    engine.controle_coherence_etiquette(dpe.find('logement'), report)

    assert (len(report.error_software) == 1)

    report = ReportDPE()

    dpe.find('*//ep_conso/ep_conso_5_usages_m2').text = '190'
    dpe.find('*//ep_conso/classe_bilan_dpe').text = 'D'
    engine.controle_coherence_etiquette(dpe.find('logement'), report)

    assert (len(report.error_software) == 0)

    dpe.find('*//classe_emission_ges').text = "E"
    dpe.find('*//emission_ges_5_usages_m2').text = "50"

    engine.controle_coherence_etiquette(dpe.find('logement'), report)

    assert (len(report.error_software) == 1)

    report = ReportDPE()
    dpe.find('*//classe_emission_ges').text = "D"
    engine.controle_coherence_etiquette(dpe.find('logement'), report)
    assert (len(report.error_software) == 2)


def test_controle_coherence_table_valeur_enum():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    self = engine
    logement = dpe.find('logement')
    self.controle_coherence_table_valeur_enum(logement, report)

    assert (len(report.error_software) == 17)

    all_tv_found = list()
    for tv in self.valeur_table_dict:
        all_tv_found.extend(dpe.iterfind(f'*//{tv}'))

    for tv in all_tv_found:
        parent = tv.getparent()
        parent_dict = element_to_value_dict(parent)
        name = tv.tag
        value = convert_xml_text(tv.text)
        if value is not None:
            related_properties = {k: v for k, v in self.valeur_table_dict[name][value].items() if v == v}
            related_enums = {k: v for k, v in related_properties.items() if k.startswith('enum')}
            for related_enum_name, admissible_values in related_enums.items():
                current_parent = parent
                current_parent_dict = parent_dict
                # if enum methode application on va chercher dans caracteristique generale
                if related_enum_name == 'enum_methode_application_dpe_log_id':
                    current_parent = dpe.find('*//caracteristique_generale')
                    current_parent_dict = element_to_value_dict(current_parent)
                # if zone climatique
                elif related_enum_name == 'enum_zone_climatique_id':
                    current_parent = dpe.find('*//meteo')
                    current_parent_dict = element_to_value_dict(current_parent)
                # if not in direct parent getting grand-parent
                elif related_enum_name not in parent_dict:
                    current_parent = parent.getparent().getparent()
                    current_parent_dict = element_to_value_dict(current_parent)

                if related_enum_name in current_parent_dict:
                    if current_parent_dict[related_enum_name] not in admissible_values:
                        enum = current_parent.find(related_enum_name)
                        enum.text = str(admissible_values[0])

    report = ReportDPE()

    self = engine

    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)

    assert (len(report.error_software) == 12)

    for mur in dpe.iterfind('*//mur/donnee_entree'):
        mur.find('enum_materiaux_structure_mur_id').text = '1'
        mur.find('tv_umur_id').text = '3'

    report = ReportDPE()

    self = engine

    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)
    assert (len(report.error_software) == 12 + 3 * 2)

    dpe = etree.parse(f, parser)
    report = ReportDPE()

    ets_collection = dpe.find('*//enveloppe/ets_collection')
    ets = etree.SubElement(ets_collection, 'ets')
    de = etree.SubElement(ets, 'donnee_entree')

    set_xml_values_from_dict(de, {'tv_coef_reduction_deperdition_id': 265,
                                  'enum_cfg_isolation_lnc_id': 6})
    di = etree.SubElement(ets, 'donnee_intermediaire')

    set_xml_values_from_dict(di, {'bver': 0.95})
    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)
    assert (len(report.error_software) == 17)
    dpe.find('*//ets/donnee_entree/enum_cfg_isolation_lnc_id').text = '1'
    report = ReportDPE()
    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)
    assert (len(report.error_software) == 18)

    # test ajout chauffage mixte
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_methode_application_dpe_log_id').text = '30'
    report = ReportDPE()
    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)
    dpe.find('*//installation_chauffage/donnee_entree/enum_type_installation_id').text = '1'
    self.controle_coherence_table_valeur_enum(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)


def test_controle_coherence_variables_requises():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    self = engine
    report = ReportDPE()

    engine.controle_coherence_variables_requises(dpe, report)

    assert (len(report.error_software) == 0)

    for control_varname in self.var_req_dict:
        control_vars = dpe.iterfind(f'//{control_varname}')
        for control_var in control_vars:
            for enum_id in self.var_req_dict[control_varname]:
                for var in [el for el in re.split('[(),|]', self.var_req_dict[control_varname][enum_id]) if el != '']:
                    control_var.text = str(enum_id)
                    element = control_var.getparent()
                    report = ReportDPE()
                    create_sub_el(element, var, 1)
                    engine.controle_coherence_variables_requises(dpe, report)
                    report = ReportDPE()
                    remove_sub_el(element, var)
                    engine.controle_coherence_variables_requises(dpe, report)


def test_controle_coherence_mutually_exclusive():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    self = engine
    report = ReportDPE()

    de = dpe.find('*//mur/donnee_entree')
    s = etree.SubElement(de, 'umur_saisi')
    s.text = str(1)
    engine.controle_coherence_mutually_exclusive(dpe.find('logement'), report)
    assert (len(report.error_software) == 2)


def test_controle_coherence_correspondance_saisi_value():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    self = engine
    report = ReportDPE()

    de = dpe.find('*//mur/donnee_entree')
    s = etree.SubElement(de, 'umur_saisi')
    s.text = str(1)

    de = dpe.find('*//installation_chauffage/donnee_entree')
    s = etree.SubElement(de, 'fch_saisi')
    s.text = str(1)
    engine.controle_coherence_correspondance_saisi_value(dpe.find('logement'), report)
    assert (len(report.error_software) == 2)
    assert (report.error_software[0]['thematique'] == 'bad_value')
    assert (report.error_software[1]['thematique'] == 'missing_required_element')


def test_controle_coherence_tv_values_simple():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    report = ReportDPE()

    self = engine

    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)

    assert (len(report.error_software) == 8)
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    ets_collection = dpe.find('*//enveloppe/ets_collection')
    ets = etree.SubElement(ets_collection, 'ets')
    de = etree.SubElement(ets, 'donnee_entree')

    set_xml_values_from_dict(de, {'tv_coef_reduction_deperdition_id': 265,
                                  'enum_cfg_isolation_lnc_id': 6})
    di = etree.SubElement(ets, 'donnee_intermediaire')

    set_xml_values_from_dict(di, {'bver': 0.95})
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 8)
    dpe.find('*//ets/donnee_intermediaire/bver').text = '0.7'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 9)

    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)
    assert (len(report.warning_software) == 0)
    dpe.find('*//coef_transparence_ets').text = '2'
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_umur_id')
    tvu.getparent().getparent().find('*//umur').text = '0.1'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_upb_id')
    tvu.getparent().getparent().find('*//upb').text = '0.1'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 2)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_uph_id')
    tvu.getparent().getparent().find('*//uph').text = '0.1'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 3)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_upb0_id')
    tvu.getparent().getparent().find('*//upb0').text = '4.4443'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 4)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_uph0_id')
    tvu.getparent().getparent().find('*//uph0').text = '4.4443'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 5)
    assert (len(report.warning_software) == 1)
    tvu = dpe.find('*//tv_umur0_id')
    tvu.getparent().getparent().find('*//umur0').text = '4.4443'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 5)
    assert (len(report.warning_software) == 1)

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    tvu = dpe.find('*//tv_uph_id')
    uph0 = tvu.getparent().getparent().find('*//uph0')
    tvu.getparent().getparent().find('donnee_intermediaire').remove(uph0)
    tvu.getparent().getparent().find('*//uph').text = '0.1'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    tvsw = dpe.find('*//tv_sw_id')
    tvsw.getparent().getparent().find('*//sw').text = '2.99'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)

    assert (len(report.report['erreur_logiciel']) == 1)

    tvsw.getparent().find('double_fenetre').text = '1'

    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)

    assert (len(report.report['erreur_logiciel']) == 0)

    for tv_value in ['tv_rendement_distribution_ecs_id', 'tv_rendement_distribution_ch_id', "tv_rendement_emission_id",
                     "tv_rendement_regulation_id", 'tv_seer_id', 'tv_rendement_generation_id']:
        print(f'TV VALUE {tv_value}')
        f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_reseau_chaleur.xml'))  # n'utilise que des tv pour les rendements
        dpe = etree.parse(f, parser)
        dpe.find(f'*//{tv_value}').text = '2'
        report = ReportDPE()
        self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
        for erreur_logiciel in report.report['erreur_logiciel']:
            print(erreur_logiciel['message'])
        assert (len(report.report['erreur_logiciel']) == 1)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    dpe.find(f'*//tv_seer_id').text = '2'
    dpe.find(f'*//eer').text = '6.175'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.report['erreur_logiciel']) == 0)
    dpe.find(f'*//tv_seer_id').text = '1'
    dpe.find(f'*//eer').text = '3.6'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.report['erreur_logiciel']) == 0)

    # test doublage

    # test doublage

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    mur_avec_doublage = list(dpe.iterfind('*//mur'))[1]
    # mur_avec_doublage.find('*//umur').text = '0.77'
    # report = Report()
    # self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    # assert (len(report.report['erreur_logiciel']) == 1)
    # mur_avec_doublage.find('*//umur').text = '0.94'
    # report = Report()
    # self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    # assert (len(report.report['erreur_logiciel']) == 0)
    # cas avec une période d'isolation récente on vérifie que la saisie par tv défaut passe
    mur_avec_doublage.find('*//umur').text = '0.4'
    mur_avec_doublage.find('*//tv_umur_id').text = '42'
    report = ReportDPE()
    self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    assert (len(report.report['erreur_logiciel']) == 0)
    # # cas avec une période d'isolation récente on vérifie que la saisie par tv défaut passe avec application du doublage
    # mur_avec_doublage.find('*//umur').text = '0.36'
    # mur_avec_doublage.find('*//tv_umur_id').text = '42'
    # report = Report()
    # self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    # assert (len(report.report['erreur_logiciel']) == 0)
    # # cas avec une période d'isolation récente on vérifie que la saisie est rejetée si U=U0
    # mur_avec_doublage.find('*//umur').text = '0.94'
    # mur_avec_doublage.find('*//tv_umur_id').text = '42'
    # report = Report()
    # self.controle_coherence_tv_values_simple(dpe.find('logement'), report)
    # assert (len(report.report['erreur_logiciel']) == 1)


def test_paroi_polycarbonate():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    dpe.find(f'*//enum_type_pose_id').text = '4'
    dpe.find(f'*//enum_type_baie_id').text = '3'
    report = engine.run_controle_coherence(dpe)
    assert (len(report['erreur_logiciel']) == 3)
    parent = dpe.find(f'*//enum_type_baie_id').getparent()
    parent.remove(parent.find('tv_ug_id'))
    dpe.find(f'*//tv_uw_id').text = '3'
    dpe.find(f'*//tv_sw_id').text = '3'
    dpe.find(f'*//sw').text = '0.4'
    dpe.find(f'*//enum_type_materiaux_menuiserie_id').text = '2'
    report = engine.run_controle_coherence(dpe)
    assert (len(report['erreur_logiciel']) == 0)


def test_controle_coherence_structure_installation_chauffage():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    report = ReportDPE()

    self = engine
    self.controle_coherence_structure_installation_chauffage(dpe.find('logement'), report)

    assert (len(report.warning_software) == 0)
    gen = dpe.find('*//generateur_chauffage')
    gen_copy = copy.deepcopy(gen)

    gen_coll = gen.getparent()

    gen_coll.append(gen_copy)
    self = engine
    self.controle_coherence_structure_installation_chauffage(dpe.find('logement'), report)
    assert (len(report.warning_software) == 1)
    dpe.find('*//emetteur_chauffage//enum_lien_generateur_emetteur_id').text = '3'
    self.controle_coherence_structure_installation_chauffage(dpe.find('logement'), report)
    assert (len(report.error_software) == 2)
    report = ReportDPE()
    gen.find('*//enum_type_generateur_ch_id').text = '144'
    self.controle_coherence_structure_installation_chauffage(dpe.find('logement'), report)
    assert (len(report.warning_software) == 0)


def test_controle_coherence_surface():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    report = ReportDPE()

    cg = dpe.find('*//caracteristique_generale')

    dpe.find('*//enum_methode_application_dpe_log_id').text = "6"

    etree.SubElement(cg, 'surface_habitable_immeuble').text = '45'
    engine.controle_coherence_surfaces(dpe.find('logement'), report)

    assert (len(report.error_software) == 3)

    etree.SubElement(cg, 'surface_tertiaire_immeuble').text = '45'
    report = ReportDPE()
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)

    report = ReportDPE()
    dpe.find('*//enum_methode_application_dpe_log_id').text = "1"
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)
    report = ReportDPE()
    dpe.find('*//surface_habitable_logement').text = "12"
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) == 3)
    dpe.find('*//mur/donnee_entree/surface_aiu').text = '0.1'
    report = ReportDPE()
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) == 4)

    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appartement_a_partir_immeuble_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)
    # si on passe en appt normal toutes les surfaces sont des surfaces d'immeuble donc erreur
    dpe.find('*//enum_methode_application_dpe_log_id').text = '2'
    report = ReportDPE()
    engine.controle_coherence_surfaces(dpe.find('logement'), report)
    assert (len(report.error_software) >= 0)


def test_controle_coherence_energie():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)

    report = ReportDPE()

    engine.controle_coherence_energie(dpe.find('logement'), report)

    assert (len(report.error_software) == 0)

    dpe.find('*//generateur_chauffage/donnee_entree/enum_type_energie_id').text = "13"

    dpe.find('*//sortie_par_energie/enum_type_energie_id').text = "9"

    engine.controle_coherence_energie(dpe.find('logement'), report)

    assert (len(report.error_software) == 3)

    dpe = etree.parse(f, parser)
    report = ReportDPE()

    # si les consommations en sortie sont = 0 -> warning
    dpe.find('*//sortie_par_energie/conso_ecs').text = "0"
    dpe.find('*//sortie_par_energie/conso_ch').text = "0"

    engine.controle_coherence_energie(dpe.find('logement'), report)

    assert (len(report.error_software) == 0)
    assert (len(report.warning_software) == 2)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    for el in dpe.iterfind('*//climatisation/donnee_entree/enum_type_energie_id'):
        el.text = "12"

    engine.controle_coherence_energie(dpe.find('logement'), report)

    assert (len(report.error_software) == 2)


def test_controle_coherence_rset_rsee():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_2_neuf_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_rset_rsee(dpe, report)
    assert (len(report.error_software) == 0)


def test_controle_coherence_hors_methode():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_hors_methode(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)

    dpe.find('*//enum_materiaux_structure_mur_id').text = "21"
    report = ReportDPE()
    engine.controle_coherence_hors_methode(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)


def test_controle_coherence_pont_thermique():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)
    report = ReportDPE()
    for mur in dpe.iterfind('*//mur//enum_type_isolation_id'):
        mur.text = "1"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)
    report = ReportDPE()
    dpe.find('*//enum_periode_construction_id').text = "7"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)

    dpe = etree.parse(f, parser)
    dpe.find('*//enum_methode_application_dpe_log_id').text = "1"
    report = ReportDPE()
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    dpe.find('*//enveloppe').remove(dpe.find('*//enveloppe/pont_thermique_collection'))
    report = ReportDPE()
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    report = ReportDPE()
    dpe = etree.parse(f, parser)
    for el in dpe.iterfind('*//pont_thermique/donnee_entree/tv_pont_thermique_id'):
        id_ = int(el.text)
        id_ += 5
        el.text = str(id_)
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    report = ReportDPE()
    dpe = etree.parse(f, parser)
    dpe.find('*//pont_thermique/donnee_entree/tv_pont_thermique_id').text = "22"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    report = ReportDPE()
    dpe.find('*//pont_thermique/donnee_entree/tv_pont_thermique_id').text = "42"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    # test ossature bois
    dpe.find('*//pont_thermique/donnee_entree/tv_pont_thermique_id').text = "69"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    report = ReportDPE()
    dpe.find('*//mur/donnee_entree/enum_materiaux_structure_mur_id').text = "25"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)
    dpe.find('*//pont_thermique/donnee_entree/tv_pont_thermique_id').text = "66"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    assert ('ossature' in report.warning_input[0]['message'])

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    dpe.find('*//porte//enum_type_pose_id').text = "3"
    report = ReportDPE()
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    report = ReportDPE()
    dpe.find('*//pont_thermique//tv_pont_thermique_id').text = "76"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 3)
    report = ReportDPE()
    dpe.find('*//porte//enum_type_pose_id').text = "1"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 3)
    report = ReportDPE()
    dpe.find('*//porte//enum_type_pose_id').text = "2"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)
    report = ReportDPE()
    dpe.find('*//porte//enum_type_pose_id').text = "1"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 3)
    report = ReportDPE()
    dpe.find('*//baie_vitree//enum_type_pose_id').text = "2"
    engine.controle_coherence_pont_thermique(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)


def test_controle_coherence_existence_composants():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_existence_composants(dpe.find('logement'), report)

    assert (len(report.warning_input) == 0)
    dpe.find('*//enum_methode_application_dpe_log_id').text = "1"
    methode_dpe = int(dpe.find('*//enum_methode_application_dpe_log_id').text)
    type_batiment = engine.enum_table['methode_application_dpe_log'].loc[methode_dpe].type_batiment
    for element in expected_components[type_batiment]:
        collection = dpe.find(f'*//{element}_collection')
        collection.getparent().remove(collection)
    engine.controle_coherence_existence_composants(dpe.find('logement'), report)

    assert (len(report.warning_input) == 10)


def test_controle_coherence_enveloppe():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)

    type_isolation = [el for el in dpe.iterfind('*//enum_type_isolation_id')]
    type_isolation_lnc = [el for el in type_isolation if el.getparent().find('enum_cfg_isolation_lnc_id') is not None]
    type_isolation_lnc[0].getparent().find('enum_type_isolation_id').text = '4'
    report = ReportDPE()
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    dpe = etree.parse(f, parser)
    type_isolation = [el for el in dpe.iterfind('*//enum_type_isolation_id')]
    type_isolation_lnc = [el for el in type_isolation if el.getparent().find('enum_cfg_isolation_lnc_id') is not None]
    type_isolation_lnc[0].getparent().find('enum_type_isolation_id').text = '1'
    dpe.find('*//enum_periode_construction_id').text = '7'
    report = ReportDPE()
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    dpe.find('*//enum_periode_construction_id').text = '1'
    report = ReportDPE()
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)
    de = dpe.find('*//mur/donnee_entree')
    enum_periode_isolation_id = etree.SubElement(de, 'enum_periode_isolation_id')
    enum_periode_isolation_id.text = '1'
    dpe.find('*//enum_periode_construction_id').text = '2'
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 1)
    report = ReportDPE()
    dpe.find('*//enum_type_adjacence_id').text = '3'
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 4)

    report = ReportDPE()
    dpe.find('*//b').text = '0'
    aue = dpe.find('*//surface_aue')
    aue.text = '0.01'
    aue.getparent().getparent().find('*//b').text = '0'
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)

    assert (len(report.warning_input) == 6)
    report = ReportDPE()
    dpe.find('*//b').text = '1'
    tv = dpe.find('*//tv_coef_reduction_deperdition_id')
    tv.getparent().remove(tv)
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    # for w in report.warning_input:
    #     print(w['message'])
    assert (len(report.warning_input) == 6)

    # test surface non déperditive.
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    dpe.find('*//b').text = '0'
    dpe.find('*//tv_coef_reduction_deperdition_id').text = '283'
    engine.controle_coherence_enveloppe(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)

    # test matériaux auto isolant qui ne déclenchent plus le warning sur la cohérence d'isolation entre calcul b et déclaration.

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = engine.run_controle_coherence(dpe)
    assert (len(report['warning_saisie']) == 6)

    mur_adj = list(dpe.iterfind('*//mur'))[1]
    mur_adj.find('*//enum_materiaux_structure_mur_id').text = '26'
    mur_adj = list(dpe.iterfind('*//mur'))[2]
    mur_adj.find('*//enum_materiaux_structure_mur_id').text = '24'
    report = engine.run_controle_coherence(dpe)
    assert (len(report['warning_saisie']) == 4)


def test_controle_coherence_systeme():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_systeme(dpe.find('logement'), report)
    assert (len(report.warning_input) == 0)
    dpe.find('*//enum_periode_construction_id').text = '8'

    dpe.find('*//enum_type_ventilation_id').text = '3'
    engine.controle_coherence_systeme(dpe.find('logement'), report)

    assert (len(report.warning_input) == 1)
    dpe.find('*//enum_type_generateur_ch_id').text = '83'
    di = dpe.find('*//generateur_chauffage/donnee_intermediaire')
    set_xml_values_from_dict(di, {'rpn': 0.9,
                                  'rpint': 0.81})
    report = ReportDPE()
    engine.controle_coherence_systeme(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)

    dpe.find('*//enum_type_generateur_ch_id').text = '87'
    set_xml_values_from_dict(di, {'rpn': 0.3,
                                  'rpint': 0.81})
    report = ReportDPE()
    engine.controle_coherence_systeme(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)

    dpe.find('*//enum_classe_inertie_id').text = '4'
    report = ReportDPE()
    engine.controle_coherence_systeme(dpe.find('logement'), report)
    assert (len(report.warning_input) == 3)

    dpe = etree.parse(f, parser)
    dpe.find('*//enum_type_generateur_ch_id').text = '55'
    dpe.find('*//enum_methode_application_dpe_log_id').text = '1'
    dpe.find('*//enum_type_generateur_ecs_id').text = '24'
    report = ReportDPE()
    engine.controle_coherence_systeme(dpe.find('logement'), report)
    assert (len(report.warning_input) == 2)


def test_generateur_cascade_sans_priorite():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_generation_cascade.xml'))
    dpe = etree.parse(f, parser)
    report = engine.run_controle_coherence(dpe)
    nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
    assert (report['validation_xsd']['valid'] == True)
    assert (nb_errors == 0)
    L_warning = len(report['warning_logiciel']) + len(report['warning_saisie'])
    for el in dpe.iterfind('*//priorite_generateur_cascade'):
        el.text = '1'
    for el in dpe.iterfind('*//conso_ch'):
        el.text = '1'
    for el in dpe.iterfind('*//conso_ch_depensier'):
        el.text = '1'
    for el in dpe.iterfind('*//rendement_generation'):
        el.text = '1'
    report = engine.run_controle_coherence(dpe)
    nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
    assert (report['validation_xsd']['valid'] == True)
    assert (nb_errors == 0)
    L_warning_sans_priorite = len(report['warning_logiciel']) + len(report['warning_saisie'])

    assert (L_warning_sans_priorite == L_warning)


def test_controle_coherence_consommation_0_generateur_installation():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_generation_cascade.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()

    for prop in ['rendement_generation', 'conso_ch_depensier', 'conso_ch']:
        for el in dpe.iterfind(f'*//generateur_chauffage//{prop}'):
            el.text = str(0)

    engine.controle_coherence_consommation_0_generateur_installation(dpe.find('logement'), report)
    assert (len(report.error_software) == 3)

    dpe.find('*//enum_cfg_installation_ch_id').text = '10'
    report = ReportDPE()
    engine.controle_coherence_consommation_0_generateur_installation(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)

    dpe.find('*//enum_cfg_installation_ch_id').text = '1'
    dpe.find('*//priorite_generateur_cascade').text = '2'
    report = ReportDPE()
    engine.controle_coherence_consommation_0_generateur_installation(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)

    # TESTS concernants les fch et fecs =1

    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_ecs_solaire_expert.xml'))
    dpe = etree.parse(f, parser)

    # si les consommations d'ecs sont 0 alors erreur
    dpe.find('*//installation_ecs/donnee_intermediaire/conso_ecs').text = '0'
    dpe.find('*//installation_ecs/donnee_intermediaire/conso_ecs_depensier').text = '0'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 2)

    # si fecs = 1 plus d'erreur

    dpe.find('*//installation_ecs/donnee_intermediaire/fecs').text = '1'
    dpe.find('*//installation_ecs/donnee_entree/fecs_saisi').text = '1'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 0)

    dpe = etree.parse(f, parser)  # reinit

    # si les consommations d'ecs sont 0 alors erreur
    dpe.find('*//generateur_ecs/donnee_intermediaire/conso_ecs').text = '0'
    dpe.find('*//generateur_ecs/donnee_intermediaire/conso_ecs_depensier').text = '0'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 2)

    # si fecs = 1 plus d'erreur

    dpe.find('*//installation_ecs/donnee_intermediaire/fecs').text = '1'
    dpe.find('*//installation_ecs/donnee_entree/fecs_saisi').text = '1'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 0)

    dpe = etree.parse(f, parser)  # reinit

    # si les consommations d'ecs sont 0 alors erreur
    dpe.find('*//installation_chauffage/donnee_intermediaire/conso_ch').text = '0'
    dpe.find('*//installation_chauffage/donnee_intermediaire/conso_ch_depensier').text = '0'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 2)

    # si fecs = 1 plus d'erreur

    dpe.find('*//installation_chauffage/donnee_intermediaire/fch').text = '1'
    dpe.find('*//installation_chauffage/donnee_entree/fch_saisi').text = '1'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 0)

    dpe = etree.parse(f, parser)  # reinit

    # si les consommations de chauffage sont 0 alors erreur
    dpe.find('*//generateur_chauffage/donnee_intermediaire/conso_ch').text = '0'
    dpe.find('*//generateur_chauffage/donnee_intermediaire/conso_ch_depensier').text = '0'
    # suppression de la sortie par énergie gaz car plus aucune consommation de gaz non nulle.
    sortie_par_energie_gaz = list(dpe.iterfind('*//sortie_par_energie'))[1]
    sortie_par_energie_gaz.getparent().remove(sortie_par_energie_gaz)
    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 2)

    # si fecs = 1 plus d'erreur

    dpe.find('*//installation_chauffage/donnee_intermediaire/fch').text = '1'
    dpe.find('*//installation_chauffage/donnee_entree/fch_saisi').text = '1'

    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 0)

    # CAS BESOIN CHAUFFAGE 0

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    for prop in ['rendement_generation', 'conso_ch_depensier', 'conso_ch']:
        for el in dpe.iterfind(f'*//generateur_chauffage//{prop}'):
            el.text = str(0)
    report = engine.run_controle_coherence(dpe)
    # les 3 erreurs 0 + l'erreur de conso sortie non cohérente
    assert (len(report['erreur_logiciel']) == 4)

    for prop in ['besoin_ch_depensier', 'besoin_ch']:
        for el in dpe.iterfind(f'*//installation_chauffage//{prop}'):
            el.text = '0'

    report = engine.run_controle_coherence(dpe)
    # l'erreur de conso sortie non cohérente + rendement 0
    assert (len(report['erreur_logiciel']) == 2)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    for prop in ['conso_ch_depensier', 'conso_ch']:
        for el in dpe.iterfind(f'*//generateur_chauffage//{prop}'):
            el.text = str(0)

    report = engine.run_controle_coherence(dpe)
    # les 2 erreurs 0 + l'erreur de conso sortie non cohérente
    assert (len(report['erreur_logiciel']) == 3)

    for prop in ['besoin_ch_depensier', 'besoin_ch']:
        for el in dpe.iterfind(f'*//installation_chauffage//{prop}'):
            el.text = '0'

    report = engine.run_controle_coherence(dpe)
    # l'erreur de conso sortie non cohérente
    assert (len(report['erreur_logiciel']) == 1)

    for prop in ['conso_ch', 'conso_ch_depensier']:
        for el in dpe.iterfind(f'*//sortie//{prop}'):
            el.text = '0'
    report = engine.run_controle_coherence(dpe)

    # l'erreur de conso sortie non cohérente

    assert (len(report['erreur_logiciel']) == 1)

    # suppression de conso sortie non cohérente

    sortie_gaz = list(dpe.iterfind('*//sortie_par_energie'))[1]
    sortie_gaz.getparent().remove(sortie_gaz)

    report = engine.run_controle_coherence(dpe)
    assert (len(report['erreur_logiciel']) == 0)


def test_controle_coherence_cle_repartition_dpe_appartement():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    dpe.find('*//enum_methode_application_dpe_log_id').text = '13'  # passage en dpe à l'appartement
    engine.controle_coherence_cle_repartition_dpe_appartement(dpe.find('logement'), report)
    assert (len(report.error_software) == 5)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appartement_a_partir_immeuble_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_cle_repartition_dpe_appartement(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)


def test_forbid_req():
    engine = EngineDPE()

    self = engine

    for k, v in self.var_req_dict.items():
        for id_, var in v.items():
            if id_ in self.var_forbid_dict.get(k, {}):
                var = re.split('[(),|]', var)
                var_forbid = re.split('[(),|]', self.var_forbid_dict[k][id_])
                if len(set(var) & set(var_forbid)) > 0:
                    raise Exception('{set(var)&set(var_forbid)} both required and forbid')


def test_controle_coherence_conso_nulle():
    _set_version_dpe_to_valid_dates()
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    sortie = dpe.find('*//sortie')
    for el in ['ef_conso/conso_ecs', 'ef_conso/conso_ecs_depensier', 'ef_conso/conso_ch', 'ef_conso/conso_ch_depensier',
               'ep_conso/ep_conso_ecs', 'ep_conso/ep_conso_ecs_depensier', 'ep_conso/ep_conso_ch', 'ep_conso/ep_conso_ch_depensier',
               'emission_ges/emission_ges_ecs', 'emission_ges/emission_ges_ecs_depensier', 'emission_ges/emission_ges_ch', 'emission_ges/emission_ges_ch_depensier',
               'cout/cout_ecs', 'cout/cout_ecs_depensier', 'cout/cout_ch', 'cout/cout_ch_depensier',
               ]:
        sortie.find(el).text = '0'
    output = engine.run_controle_coherence(dpe)
    assert (output['validation_xsd']['valid'] == True)


def test_controle_coherence_fch_fecs_saisi():
    engine = EngineDPE()

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_ecs_solaire_expert.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//fecs_saisi').text = '0.42'
    dpe.find('*//fch_saisi').text = '0.42'
    report = engine.run_controle_coherence(dpe)

    assert (len(report['erreur_logiciel']) == 2)

# TODO : @Antoine on ne doit pas l'appliquer lui à l'audit ?
def test_controle_coherence_ref_dpe_immeuble():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appartement_a_partir_immeuble_valid.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_ref_dpe_immeuble(dpe, report)
    assert (len(report.error_input) == 0)
    el = dpe.find('*//dpe_immeuble_associe')
    el.getparent().remove(el)
    report = ReportDPE()
    engine.controle_coherence_ref_dpe_immeuble(dpe, report)
    assert (len(report.error_input) == 1)


def test_controle_coherence_unicite_reference():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_unicite_reference(dpe.find('logement'), report)
    print(report.error_software)
    assert (len(report.error_software) == 0)
    for el in dpe.iterfind('*//reference'):
        el.text = 'toto'
    engine.controle_coherence_unicite_reference(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)


def test_controle_coherence_modele_methode_application():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)

    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    report = ReportDPE()
    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    engine.controle_coherence_modele_methode_application(logement, report)
    assert (len(report.error_software) == 0)
    report = ReportDPE()
    # test incohérence méthode
    dpe.find('*//enum_modele_dpe_id').text = '2'
    engine.controle_coherence_modele_methode_application(logement, report)
    assert (len(report.error_software) == 1)
    # test remise en cohérence neuf
    report = ReportDPE()
    dpe.find('*//enum_methode_application_dpe_log_id').text = '15'
    engine.controle_coherence_modele_methode_application(logement, report)
    assert (len(report.error_software) == 0)

    engine = EngineAudit()
    for valid_example in VALID_CASES_AUDIT:
        f = str((engine.mdd_path / 'exemples_metier' / valid_example))
        audit = etree.parse(f, parser)
        report = ReportAudit()
        # test non homogénéité des méthodes d'application -> Pour l'instant le modèle audit refuse les appartements générés à partir de l'immmeuble
        audit.find('*//enum_methode_application_dpe_log_id').text = '10'
        logement = audit.find('*//logement')
        engine.controle_coherence_modele_methode_application(logement, report)
        assert (len(report.error_software) == 1)
        # test non homogénéité des méthodes d'application -> Pour l'instant le modèle audit refuse tout le neuf par défaut
        audit.find('*//enum_methode_application_dpe_log_id').text = '15'
        logement = audit.find('*//logement')
        report = ReportAudit()
        engine.controle_coherence_modele_methode_application(logement, report)
        assert (len(report.error_software) == 1)
        # test homogénéité des méthodes d'application -> Pour l'instant le modèle audit refuse tout le neuf par défaut
        iter = 0
        for el in audit.iterfind('*//enum_methode_application_dpe_log_id'):
            el.text = '15'
            iter += 1

        report = ReportAudit()
        for logement in audit.iterfind('*//logement'):
            engine.controle_coherence_modele_methode_application(logement, report)
        assert (len(report.error_software) == iter)


def test_controle_coherence_calcul_echantillonage():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_calcul_echantillonage(dpe.find('logement'), report)
    assert (len(report.error_software) == 0)

    # test que enum_calcul_echantillonage doit déclencher une erreur si non déclaré.
    enum_calcul_echantillonnage_id = dpe.find('*//enum_calcul_echantillonnage_id')
    enum_calcul_echantillonnage_id.getparent().remove(enum_calcul_echantillonnage_id)
    report = ReportDPE()
    engine.controle_coherence_calcul_echantillonage(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)


def test_controle_coherence_logement_visite():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_logement_visite(dpe, report)
    assert (len(report.error_software) == 0)

    # test que enum_calcul_echantillonage doit déclencher une erreur si non déclaré.

    to_remove = dpe.find('dpe_immeuble')
    to_remove.getparent().remove(to_remove)
    report = ReportDPE()
    engine.controle_coherence_logement_visite(dpe.find('logement'), report)
    assert (len(report.error_software) == 1)

    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))  # TODO : @Antoine Dois-je créer un cas test immeuble ?
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_logement_visite(audit, report)
    assert (len(report.error_software) == 0)

    # Change la méthode d'un logement en "dpe immeuble collectif", afin d'ajouter 1 erreur : méthode immeuble sans DPE_immeuble renseigné !
    logement = list(audit.iterfind('*//logement'))[0]
    report = ReportAudit()
    logement.find('*//enum_methode_application_dpe_log_id').text = '6'
    engine.controle_coherence_logement_visite(audit, report)
    assert (len(report.error_software) == 1)


def test_controle_coherence_double_fenetre():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_double_fenetre.xml'))
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_double_fenetre(dpe, report)
    assert (len(report.error_software) == 0)
    # si double fenetre sans sous structure -> erreur
    el = dpe.find('*//baie_vitree_double_fenetre')
    el.getparent().remove(el)
    report = ReportDPE()
    engine.controle_coherence_double_fenetre(dpe, report)
    assert (len(report.error_software) == 1)
    # LE DPE 2.2 N EST PLUS ACCEPTÉ !
    ## si ancienne version pas d'erreur
    # dpe.find('*//enum_version_id').text = '2.2'
    # report = engine.run_controle_coherence(dpe)
    # assert (len(report['erreur_logiciel']) == 0)

    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v1.1_valid_double_fenetre.xml'))
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_double_fenetre(audit, report)
    assert (len(report.error_software) == 0)
    # si double fenetre sans sous structure -> erreur
    el = audit.find('*//baie_vitree_double_fenetre')
    el.getparent().remove(el)
    report = ReportAudit()
    engine.controle_coherence_double_fenetre(audit, report)
    assert (len(report.error_software) == 1)


def test_controle_coherence_pveilleuse():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_anomalie_veilleusev2.2.xml'))
    # cas de base génère l'erreur
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 1)

    # on met une chaudière neuve -> plus d'erreur
    dpe.find('*//enum_type_generateur_ch_id').text = '97'
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 0)

    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_tout_chaudiere.xml'))
    # cas valide sans erreur
    dpe = etree.parse(f, parser)
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 0)
    # suppression des veilleuses lèvent des erreurs

    pveil = dpe.find('*//generateur_ecs/donnee_intermediaire/pveilleuse')
    pveil.getparent().remove(pveil)
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 1)
    pveil = dpe.find('*//generateur_chauffage/donnee_intermediaire/pveilleuse')
    pveil.getparent().remove(pveil)
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 2)
    # on met une chaudière neuve -> plus d'erreur
    dpe.find('*//enum_type_generateur_ch_id').text = '97'
    dpe.find('*//enum_type_generateur_ecs_id').text = '57'
    report = ReportDPE()
    engine.controle_coherence_presence_veilleuse(dpe, report)
    assert (len(report.warning_software) == 0)


def test_controle_coherence_arrete_reseau_chaleur():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_reseau_chaleur.xml'))
    # cas de base sans erreur
    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.error_software) == 0)
    assert (len(report.warning_input) == 0)
    el = logement.find('*//identifiant_reseau_chaleur').getparent()
    date_arrete_reseau_chaleur = el.find('date_arrete_reseau_chaleur')
    el.remove(date_arrete_reseau_chaleur)
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.error_software) == 1)

    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_reseau_chaleur.xml'))

    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    # erreur mauvaise date
    logement.find('*//date_arrete_reseau_chaleur').text = '2021-09-01'
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.error_software) == 1)

    # erreur arrêté obsolète
    logement.find('*//date_arrete_reseau_chaleur').text = '2020-10-12'
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.error_software) == 1)

    # pour les versions futures on laisse passer
    logement.find('*//date_arrete_reseau_chaleur').text = '2200-10-12'
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.error_software) == 0)

    logement.find('*//identifiant_reseau_chaleur').text = '9999C'
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.warning_input) == 0)

    logement.find('*//identifiant_reseau_chaleur').text = '9999C'
    logement.find('*//date_arrete_reseau_chaleur').text = engine.arrete_reseau_chaleur[-1]['date_arrete_reseau_chaleur']

    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    assert (len(report.warning_input) == 1)
    assert (len(report.error_software) == 0)

    logement.find('*//identifiant_reseau_chaleur').text = '9999C'
    logement.find('*//date_arrete_reseau_chaleur').text = engine.arrete_reseau_chaleur[0]['date_arrete_reseau_chaleur']
    engine.arrete_reseau_chaleur[0]['date_fin'] = '2200-01-01'
    report = ReportDPE()
    engine.controle_coherence_reseau_chaleur(logement, report)
    # le warning identifiant n'est pas levé car pour les premiers arrêtés réseau il n'y avait pas d'identifiant
    assert (len(report.warning_input) == 0)
    assert (len(report.error_software) == 0)


def test_controle_coherence_calcul_ue():
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid.xml'))
    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    report = ReportDPE()
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 0)

    # test suppression des paramètres requis déclenche erreur
    for el in ['surface_ue', 'perimetre_ue', 'ue']:
        dpe = etree.parse(f, parser)
        logement = dpe.find('logement')
        el = logement.find(f'*//{el}')
        el.getparent().remove(el)
        report = ReportDPE()
        engine.controle_coherence_calcul_ue(logement, report)
        assert (len(report.error_software) == 1)

    # on ne dit pas que l'on calcule le ue alors qu'on le fait -> erreur
    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    logement.find('*//calcul_ue').text = '0'
    report = ReportDPE()
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 1)

    # test si on est plus dans le cas d'un calcul ue on a tous les avertissements comme quoi c'est pas bon

    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    logement.find('*//plancher_bas//enum_type_adjacence_id').text = '1'
    report = ReportDPE()
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 5)

    # test si upb=upb_final on fait disparaitre une erreur

    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    logement.find('*//plancher_bas//upb').text = logement.find('*//plancher_bas//upb_final').text
    logement.find('*//plancher_bas//enum_type_adjacence_id').text = '1'
    report = ReportDPE()
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 4)
    # test suppression des paramètres ue ne déclenche plus d'erreur associée
    for el in ['surface_ue', 'perimetre_ue', 'ue']:
        logement = dpe.find('logement')
        el = logement.find(f'*//{el}')
        el.getparent().remove(el)
        report = ReportDPE()
        engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 1)

    # si on passe en calcul ue = 0 alors ca y est plus d'erreur
    logement.find('*//plancher_bas//calcul_ue').text = '0'
    report = ReportDPE()
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 0)

    # test si ue != u_plancher_bas -> erreur
    dpe = etree.parse(f, parser)
    logement = dpe.find('logement')
    logement.find('*//plancher_bas//ue').text = '1'
    engine.controle_coherence_calcul_ue(logement, report)
    assert (len(report.error_software) == 1)

def test_passage_dpe_2_point_3():
    # test de bon fonctionnement du passage à la 2.3
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    report = engine.run_controle_coherence(dpe)
    assert (report['validation_xsd']['valid'] == True)
    label_brut_avec_complement = dpe.find('*//label_brut_avec_complement')
    label_brut_avec_complement.getparent().remove(label_brut_avec_complement)
    report = engine.run_controle_coherence(dpe)
    assert (report['validation_xsd']['valid'] == False)

    # test de rétrocompatibilité 2.2
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_immeuble_1_valid_avec_reference.xml'))
    dpe = etree.parse(f, parser)
    dpe.find('*//enum_version_id').text = '2.2'
    report = engine.run_controle_coherence(dpe)
    nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
    assert (report['validation_xsd']['valid'] == True)
    assert (nb_errors == 0)


def test_passage_audit_2_1():
    _set_version_audit_to_valid_dates()
    try:
        # test de bon fonctionnement du passage à la 2.1
        engine = EngineAudit()
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.1_valid.xml'))
        audit = etree.parse(f, parser)
        report = engine.run_controle_coherence(audit)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

        # Ajout d'une erreur qui est bloquante en 2.1 (mais pas en 2.0)
        enum_derogation_ventilation_id = audit.find('*//enum_derogation_ventilation_id')
        enum_derogation_ventilation_id.getparent().remove(enum_derogation_ventilation_id)
        report = engine.run_controle_coherence(audit)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == False)

        # test de rétrocompatibilité 2.0
        audit.find('*//enum_version_audit_id').text = '2.0'
        report = engine.run_controle_coherence(audit)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

    except AssertionError as e:
        print(report['validation_xsd'])
        print(f'nombre d erreurs : {nb_errors}')
        for err in report['erreur_logiciel'] + report['erreur_saisie']:
            print("=================== ERROR =========================")
            print(err['thematique'])
            print(err['message'])
            print(err['objets_concerne'])
        print(f)
        raise e


def test_controle_coherence_type_batiment_constant():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_type_batiment_constant(audit, report)
    assert (len(report.error_software) == 0)

    # Change la méthode d'un logement en "dpe appartement individuel", afin d'ajouter 1 erreur : méthode maison ET méthode appartement ensemble !
    logement = list(audit.iterfind('*//logement'))[0]
    report = ReportAudit()
    logement.find('*//enum_methode_application_dpe_log_id').text = '2'
    engine.controle_coherence_type_batiment_constant(audit, report)
    assert (len(report.error_software) == 1)

# LE CONTROLE DE COHERENCE "controle_coherence_choix_maj_ou_remplacer" N'EST PLUS APPLIQUE SUITE A UN ROLLBACK DE LA 2.0 (voir issue gitlab #129)
# def test_controle_coherence_choix_maj_ou_remplacer():
#     engine = EngineAudit()
#     parser = etree.XMLParser(remove_blank_text=True)
#     f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
#     # cas de base valide
#     audit = etree.parse(f, parser)
#     report = ReportAudit()
#     engine.controle_coherence_choix_maj_ou_remplacer(audit, report)
#     assert (len(report.error_software) == 0)
#
#     # Ajoute audit_a_mettre_a_jour en plus de audit_a_remplacer déjà présent, afin d'ajouter 1 erreur
#     audit_a_mettre_a_jour = copy.deepcopy(audit.find('*/audit_a_remplacer'))
#     audit_a_mettre_a_jour.tag = 'audit_a_mettre_a_jour'
#     audit.find('*/audit_a_remplacer').getparent().insert(0, audit_a_mettre_a_jour)
#     report = ReportAudit()
#     engine.controle_coherence_choix_maj_ou_remplacer(audit, report)
#     assert (len(report.error_software) == 1)


def test_controle_coherence_unicite_etape_par_scenario():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_unicite_etape_par_scenario(audit, report)
    assert (len(report.error_software) == 0)

    # Duplique un logement, afin d'ajouter 1 erreur
    logement = list(audit.iterfind('*//logement'))[0]
    logement_copy = copy.deepcopy(logement)
    p = logement.getparent()
    p.insert(0, logement_copy)
    report = ReportAudit()
    engine.controle_coherence_unicite_etape_par_scenario(audit, report)
    assert (len(report.error_software) == 1)

    # Duplique un autre logement, afin d'ajouter 1 erreur
    logement = list(audit.iterfind('*//logement'))[-1]
    logement_copy = copy.deepcopy(logement)
    p = logement.getparent()
    p.insert(0, logement_copy)
    report = ReportAudit()
    engine.controle_coherence_unicite_etape_par_scenario(audit, report)
    assert (len(report.error_software) == 2)

def test_controle_coherence_presence_etat_initial():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_presence_etat_initial(audit, report)
    assert (len(report.error_software) == 0)

    # passe tous les enum_etape_id en "5": "étape intermédiaire 3" pour créer 1 erreur - pas d'etat_initial
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
        enum_etape_id.text = '5'
    report = ReportAudit()
    engine.controle_coherence_presence_etat_initial(audit, report)
    assert (len(report.error_software) == 1)
    assert report.error_software[0]['thematique'] == 'error_missing_etape'


def test_controle_coherence_scenario_multi_etapes():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_scenario_multi_etapes(audit, report)
    assert (len(report.error_input) == 0)

    # passe tous les enum_etape_id en "5": "étape intermédiaire 3" pour créer 2 erreurs - pas d'étape finale et pas d'étape première
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
        enum_etape_id.text = '5'
    report = ReportAudit()
    engine.controle_coherence_scenario_multi_etapes(audit, report)
    assert (len(report.error_input) == 2)
    assert report.error_input[0]['thematique'] == 'error_missing_etape'

    # passe tous les enum_etape_id en "1": "étape première" pour créer 1 erreur - pas d'étape finale
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
        enum_etape_id.text = '1'
    report = ReportAudit()
    engine.controle_coherence_scenario_multi_etapes(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_missing_etape'

    # passe tous les enum_etape_id en "0": "état initial" pour créer 1 erreur - pas de scénario multi étapes "principal"
    for enum_etape_id in list(audit.iterfind('*//enum_scenario_id')):
        enum_etape_id.text = '0'
    report = ReportAudit()
    engine.controle_coherence_scenario_multi_etapes(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_missing_scenario'

def test_controle_coherence_scenario_mono_etape():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_scenario_mono_etape(audit, report)
    assert (len(report.error_input) == 0)

    # passe tous les enum_etape_id en "5": "étape intermédiaire 3" pour créer 1 erreur - pas d'étape finale
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
        enum_etape_id.text = '5'
    report = ReportAudit()
    engine.controle_coherence_scenario_mono_etape(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_missing_etape'


    # passe tous les enum_scenario_id "2": "scénario en une étape \"principal\""
    for enum_scenario_id in list(audit.iterfind('*//enum_scenario_id')):
        enum_scenario_id.text = '2'
    # donne une valeur différente à tous les enum_etape_id pour créer 1 erreur - error_number_etape (trop d'étape pour ce scénario)
    i = 0
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
        enum_etape_id.text = str(i)
        i += 1
    report = ReportAudit()
    engine.controle_coherence_scenario_mono_etape(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_number_etape'

    # passe tous les enum_etape_id en "0": "état initial" pour créer 1 erreur - pas de scénario une seule étape "principal"
    for enum_etape_id in list(audit.iterfind('*//enum_scenario_id')):
        enum_etape_id.text = '0'
    report = ReportAudit()
    engine.controle_coherence_scenario_mono_etape(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_missing_scenario'

def test_controle_coherence_etape_finale():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_etape_finale(audit, report)
    assert (len(report.error_input) == 0)

    # passe toutes les etapes finales en classe DPE 'C' pour créer 2 erreurs (scénarios principaux) et au moins 1 warning (scénarios complémentaires)- pas d'atteinte de classe B pour les scénarios
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
         # if etape finale
        if enum_etape_id.text == '2':
            enum_etape_id.getparent().getparent().find('etape_travaux').find('classe_bilan_dpe').text = 'C'
    report = ReportAudit()
    engine.controle_coherence_etape_finale(audit, report)
    assert (len(report.error_input) == 2)
    assert (len(report.warning_input) > 0)
    assert report.error_input[0]['thematique'] == 'error_class_etape_finale'
    assert report.warning_input[0]['thematique'] == 'warning_class_etape_finale'

    # active la derogation technique - pas d'erreur
    audit.find('.//enum_derogation_technique_id').text = '2'
    report = ReportAudit()
    engine.controle_coherence_etape_finale(audit, report)
    assert (len(report.error_input) == 0)

    # desactive la derogation technique et ACTIVE la derograiton economique - pas d'erreur
    audit.find('.//enum_derogation_technique_id').text = '1'
    audit.find('.//enum_derogation_economique_id').text = '2'
    report = ReportAudit()
    engine.controle_coherence_etape_finale(audit, report)
    assert (len(report.error_input) == 0)

    # passe toutes les etapes finales en classe DPE 'F' pour créer 2 erreurs (scénarios principaux) et au moins 1 warning (scénarios complémentaires) - pas de saut de 2 classes pour les scénarios
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
         # if etape finale
        if enum_etape_id.text == '2':
            enum_etape_id.getparent().getparent().find('etape_travaux').find('classe_bilan_dpe').text = 'F'
    report = ReportAudit()
    engine.controle_coherence_etape_finale(audit, report)
    assert (len(report.error_input) == 2)
    assert (len(report.warning_input) > 0)
    assert report.error_input[0]['thematique'] == 'error_class_etape_finale'
    assert report.warning_input[0]['thematique'] == 'warning_class_etape_finale'

def test_controle_coherence_six_postes_travaux():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_six_postes_travaux(audit, report)
    assert (len(report.warning_input) == 0)

    # active la derogation technique - pas d'erreur
    audit.find('.//enum_derogation_technique_id').text = '2'
    report = ReportAudit()
    engine.controle_coherence_six_postes_travaux(audit, report)
    assert (len(report.warning_input) == 0)

    # desactive la derogation technique et ACTIVE la derograiton economique - pas d'erreur
    audit.find('.//enum_derogation_technique_id').text = '1'
    audit.find('.//enum_derogation_economique_id').text = '2'
    report = ReportAudit()
    engine.controle_coherence_six_postes_travaux(audit, report)
    assert (len(report.warning_input) == 0)

    # passe toutes les enum_lot_travaux_audit_id en "1" pour "mur" afin de créer 2 erreurs - 1 par scénario principal
    for etape_travaux in list(audit.iterfind('*//etape_travaux')):
        all_lot_travaux = list(etape_travaux.iterfind('*//enum_lot_travaux_audit_id'))
        for lot_travaux in all_lot_travaux:
            lot_travaux.text = '1'

    report = ReportAudit()
    engine.controle_coherence_six_postes_travaux(audit, report)
    assert (len(report.warning_input) == 2)
    assert report.warning_input[0]['thematique'] == 'warning_missing_work'

# CE CONTROLE (QUI ETAIT EN WARNING) DOIT ETRE RETIRE POUR LE 1er JANVIER 2024
#def test_controle_coherence_scenario_multi_etapes_passoire():
#    engine = EngineAudit()
#    parser = etree.XMLParser(remove_blank_text=True)
#    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
#    # cas de base valide
#    audit = etree.parse(f, parser)
#    report = ReportAudit()
#    engine.controle_coherence_scenario_multi_etapes_passoire(audit, report)
#    assert (len(report.warning_input) == 0)
#
#    # passe toutes les etapes intermédiaire en classe DPE 'D' pour créer 1 erreur - pas d'atteinte de classe C pour scénario multi étapes
#    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
#         # if etape intermédiaire
#        if enum_etape_id.text in ["3","4","5"]:
#            enum_etape_id.getparent().getparent().find('.//classe_bilan_dpe').text = 'D'
#    report = ReportAudit()
#    engine.controle_coherence_scenario_multi_etapes_passoire(audit, report)
#    assert (len(report.warning_input) == 1)
#    assert report.warning_input[0]['thematique'] == 'warning_class_etape_intermediaire'
#
#    # passe toutes les etapes intermédiaire en etapes finale - erreur : pas d'étape intermédiaire
#    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
#        # if etape intermédiaire
#        if enum_etape_id.text in ["3", "4", "5"]:
#            enum_etape_id.text = '2'
#    report = ReportAudit()
#    engine.controle_coherence_scenario_multi_etapes_passoire(audit, report)
#    assert (len(report.warning_input) == 1)
#    assert report.warning_input[0]['thematique'] == 'warning_missing_etape'
#
#    # sort le batiment de l'état passoir en état initial - Plus aucune erreur
#    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
#        # if etat initial
#        if enum_etape_id.text in ["0"]:
#            enum_etape_id.getparent().getparent().find('*//classe_bilan_dpe').text = "E"
#    report = ReportAudit()
#    engine.controle_coherence_scenario_multi_etapes_passoire(audit, report)
#    assert (len(report.warning_input) == 0)

def test_controle_coherence_seuil_3_etapes():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_seuil_3_etapes(audit, report)
    assert (len(report.warning_input) == 0)

    # passe toutes les etapes intermédiaire en classe DPE 'D' pour créer 1 erreur - pas d'atteinte de classe C pour scénario multi étapes
    for enum_scenario_id in list(audit.iterfind('*//enum_scenario_id')):
         # if not "scénario multi étapes \"principal\""
        if enum_scenario_id.text != "1":
            enum_scenario_id.text  = "1"
    report = ReportAudit()
    engine.controle_coherence_seuil_3_etapes(audit, report)
    assert (len(report.warning_input) == 1)
    assert report.warning_input[0]['thematique'] == 'warning_too_many_etape'


def test_controle_coherence_etape_premiere():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_etape_premiere(audit, report)
    assert (len(report.error_input) == 0)

    # passe toutes les etapes premières en classe DPE 'G' pour créer 2 erreurs - pas d'atteinte de classe E ET pas de gain de classe
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
         # if etape première
        if enum_etape_id.text == '1':
            enum_etape_id.getparent().getparent().find('etape_travaux').find('classe_bilan_dpe').text = 'G'
    report = ReportAudit()
    engine.controle_coherence_etape_premiere(audit, report)
    assert (len(report.error_input) == 2)
    assert report.error_input[0]['thematique'] == 'error_class_etape_finale'

    # passe toutes les etapes premières en classe DPE 'F' pour créer 1 erreurs - pas d'atteinte de classe E
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
         # if etape première
        if enum_etape_id.text == '1':
            enum_etape_id.getparent().getparent().find('etape_travaux').find('classe_bilan_dpe').text = 'F'
    report = ReportAudit()
    engine.controle_coherence_etape_premiere(audit, report)
    assert (len(report.error_input) == 1)
    assert report.error_input[0]['thematique'] == 'error_class_etape_finale'

    # passe toutes les etapes premières en classe DPE 'E' - pas d'erreurs !
    for enum_etape_id in list(audit.iterfind('*//enum_etape_id')):
         # if etape première
        if enum_etape_id.text == '1':
            enum_etape_id.getparent().getparent().find('etape_travaux').find('classe_bilan_dpe').text = 'E'
    report = ReportAudit()
    engine.controle_coherence_etape_premiere(audit, report)
    assert (len(report.error_input) == 0)

def test_controle_coherence_presence_recommandation():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_presence_recommandation(audit, report)
    assert (len(report.warning_input) == 0)

    # retire tout le contenu du text des balises 'recommandation' - warning : recommandation vides
    for recommandation_scenario in list(audit.iterfind('*//recommandation_scenario')):
        recommandation_scenario.find('recommandation').text = ''
    report = ReportAudit()
    engine.controle_coherence_presence_recommandation(audit, report)
    assert (len(report.warning_input) == 1)

    # supprime les objets recommandation_scenario - warning : pas de recommandation définie
    for recommandation_scenario in list(audit.iterfind('*//recommandation_scenario')):
        recommandation_scenario.getparent().remove(recommandation_scenario)
    report = ReportAudit()
    engine.controle_coherence_presence_recommandation(audit, report)
    assert (len(report.warning_input) == 1)

def test_controle_coherence_presence_numero_dpe():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide (le numéro DPE est manquant, avec un "audit volontaire logement")
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_presence_numero_dpe(audit, report)
    assert (len(report.warning_input) == 0)

    # passage de enum_modele_audit_id en règlementaire - 1 warning
    audit.find(f'*//enum_modele_audit_id').text = "1" # "1": "audit réglementaire logement"
    report = ReportAudit()
    engine.controle_coherence_presence_numero_dpe(audit, report)
    assert (len(report.warning_input) == 1)

def test_controle_coherence_date_visite_date_etablissement():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_date_visite_date_etablissement(audit, report)
    assert (len(report.warning_input) == 0)

    # même date pour la visite et l'etablissement de l'audit - 1 warning
    date_visite = audit.find(f'*//date_visite_auditeur').text
    audit.find(f'*//date_etablissement_audit').text = date_visite
    report = ReportAudit()
    engine.controle_coherence_date_visite_date_etablissement(audit, report)
    assert (len(report.warning_input) == 1)

    # date etablissement 1 mois plus tard que la visite - 0 warning
    audit.find(f'*//date_etablissement_audit').text = '2022-02-01'
    report = ReportAudit()
    engine.controle_coherence_date_visite_date_etablissement(audit, report)
    assert (len(report.warning_input) == 0)

    # date visite 1 an plus tard que la date d'établissement - 1 warning
    audit.find(f'*//date_visite_auditeur').text = '2023-01-01'
    report = ReportAudit()
    engine.controle_coherence_date_visite_date_etablissement(audit, report)
    assert (len(report.warning_input) == 1)


def test_controle_coherence_reference_travaux_existent():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    logement = list(audit.iterfind('*//logement'))[1]
    report = ReportAudit()
    engine.controle_coherence_reference_travaux_existent(logement, report)
    assert (len(report.error_software) == 0)

    # Cas avec une référence dans travaux_collection non présente dans le logement
    reference = logement.find('etape_travaux').find('travaux_collection')[0].find('reference_collection')[0]
    reference.text = 'ANOMALIE'
    report = ReportAudit()
    engine.controle_coherence_reference_travaux_existent(logement, report)
    assert (len(report.error_software) == 1)

def test_controle_coherence_presence_etape_travaux():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_presence_etape_travaux(logement, report)
        assert (len(report.error_software) == 0)

    # Modifie chaque logement et verifie qu'une erreur soit retournée à chaque fois.
    etape_travaux_copy = copy.deepcopy(list(audit.iterfind('*//etape_travaux'))[0])
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        enum_scenario_id = logement.find('.//caracteristique_generale').find('enum_scenario_id').text
        enum_etape_id = logement.find('.//caracteristique_generale').find('enum_etape_id').text
        logement_etat_initial = enum_scenario_id == '0' and enum_etape_id == '0'
        if logement_etat_initial:
            # insere etape_travaux pour créer une erreur
            logement.insert(0, etape_travaux_copy)
        else:
            # retire etape_travaux pour créer un erreur
            etape_travaux = logement.find('etape_travaux')
            logement.remove(etape_travaux)
        engine.controle_coherence_presence_etape_travaux(logement, report)
        assert (len(report.error_software) == 1)

def test_controle_coherence_etat_composant():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_etat_composant(logement, report)
        assert (len(report.error_software) == 0)

    # Modifie chaque logement et verifie qu'une erreur soit retournée à chaque fois.
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        enum_scenario_id = logement.find('.//caracteristique_generale').find('enum_scenario_id').text
        enum_etape_id = logement.find('.//caracteristique_generale').find('enum_etape_id').text
        logement_etat_initial = enum_scenario_id == '0' and enum_etape_id == '0'
        if logement_etat_initial:
            # change la valeur d'un enum_etat_composant_id pour créer une erreur
            list(logement.iterfind('*//enum_etat_composant_id'))[0].text = '2'
        else:
            # passe tous les enum_etat_composant_id en "1": "initial" pour créer une erreur
            for etat_composant in list(logement.iterfind('*//enum_etat_composant_id')):
                etat_composant.text = '1'
        engine.controle_coherence_etat_composant(logement, report)
        assert (len(report.error_software) == 1)

def test_controle_coherence_etape_travaux_sortie_dpe():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_etape_travaux_sortie_dpe(logement, report)
        assert (len(report.error_software) == 0)

    mapping_etape_travaux_sortie_dpe = {
        "ep_conso_5_usages_m2": "ep_conso_5_usages_m2",
        "ef_conso_5_usages_m2": "conso_5_usages_m2",
        "emission_ges_5_usages_m2": "emission_ges_5_usages_m2",
        "classe_emission_ges": "classe_emission_ges",
        "classe_bilan_dpe": "classe_bilan_dpe",
    }
    # Modifie chaque etape_travaux de chaque logement de façon légère (sous le seuil de tolérance) et vérifie qu'aucune erreur ne soit retournée
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            for etape_travaux_name in ["ep_conso_5_usages_m2", "ef_conso_5_usages_m2", "emission_ges_5_usages_m2"]:
                dpe_sortie_name = mapping_etape_travaux_sortie_dpe[etape_travaux_name]
                dpe_sortie_el = logement.find('.//sortie').find(f'.//{dpe_sortie_name}')
                etape_travaux_el = logement.find('.//etape_travaux').find(f'.//{etape_travaux_name}')
                etape_travaux_el.text = str(float(dpe_sortie_el.text) + 0.05)
            engine.controle_coherence_etape_travaux_sortie_dpe(logement, report)
            assert (len(report.error_software) == 0)

    # Modifie les classes (DPE et GES) chaque etape_travaux de chaque logement et vérifie que 2 erreurs n'ajoutent aux 3 erreurs précédentes = 5 erreurs
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            for etape_travaux_name in ["ep_conso_5_usages_m2", "ef_conso_5_usages_m2", "emission_ges_5_usages_m2"]:
                dpe_sortie_name = mapping_etape_travaux_sortie_dpe[etape_travaux_name]
                dpe_sortie_el = logement.find('.//sortie').find(f'.//{dpe_sortie_name}')
                etape_travaux_el = logement.find('.//etape_travaux').find(f'.//{etape_travaux_name}')
                etape_travaux_el.text = str(float(dpe_sortie_el.text) + 1.01)
            engine.controle_coherence_etape_travaux_sortie_dpe(logement, report)
            assert (len(report.error_software) == 3)

    # Modifie chaque etape_travaux de chaque logement de façon importante et vérifie que 3 erreurs soit retournée à chaque fois.
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            change_classe = {"A": "B", "B": "C", "C": "D", "D": "E", "E": "F", "F": "G", "G": "A"}
            for etape_travaux_name in ["classe_emission_ges","classe_bilan_dpe"]:
                dpe_sortie_name = mapping_etape_travaux_sortie_dpe[etape_travaux_name]
                dpe_sortie_el = logement.find('.//sortie').find(f'.//{dpe_sortie_name}')
                etape_travaux_el = logement.find('.//etape_travaux').find(f'.//{etape_travaux_name}')
                etape_travaux_el.text = change_classe[dpe_sortie_el.text]
            engine.controle_coherence_etape_travaux_sortie_dpe(logement, report)
            assert (len(report.error_software) == 5)

def test_controle_coherence_etape_travaux_cout():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_etape_travaux_cout(logement, report)
        assert (len(report.error_software) == 0)

    # Modifie chaque etape_travaux de chaque logement de façon légère (sous le seuil de tolérance de 100€) et vérifie qu'aucune erreur ne soit retournée
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            logement.find('.//etape_travaux').find(f'.//cout').text = str(int(logement.find('.//etape_travaux').find(f'.//cout').text) + 90)
            engine.controle_coherence_etape_travaux_cout(logement, report)
            assert (len(report.error_software) == 0)

    # Modifie chaque etape_travaux de chaque logement de façon importante (au-dessus du seuil de tolérance de 100€) - vérifie qu'une erreur soit retournée
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            logement.find('.//etape_travaux').find(f'.//cout').text = str(int(logement.find('.//etape_travaux').find(f'.//cout').text) + 200)
            engine.controle_coherence_etape_travaux_cout(logement, report)
            assert (len(report.error_software) == 1)

def test_controle_coherence_cout_nul():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_cout_nul(logement, report)
        assert (len(report.warning_input) == 0)

    # Modifie chaque etape_travaux de chaque logement de façon légère (sous le seuil de tolérance de 100€) et vérifie qu'aucune erreur ne soit retournée
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            logement.find('.//travaux_collection').find(f'.//cout').text = str(0)
            engine.controle_coherence_cout_nul(logement, report)
            assert (len(report.warning_input) == 1)

def test_controle_coherence_conso_etape_travaux():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.0_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_conso_etape_travaux(logement, report)
        assert (len(report.warning_software) == 0)

    # Modifie chaque etape_travaux de chaque logement d'un facteur 100 (correspondant à un oubli de division par la SHAB) - Une erreur doit être générée
    for logement in list(audit.iterfind('*//logement')):
        etape_travaux = logement.find('.//etape_travaux')
        if etape_travaux is not None:
            report = ReportAudit()
            conso_etape_travaux = ["ep_conso_ch_m2","ep_conso_ecs_m2","ep_conso_eclairage_m2","ep_conso_totale_auxiliaire_m2","ep_conso_fr_m2","ep_conso_5_usages_m2","ef_conso_ch_m2","ef_conso_ecs_m2","ef_conso_eclairage_m2","ef_conso_totale_auxiliaire_m2","ef_conso_fr_m2","ef_conso_5_usages_m2","emission_ges_5_usages_m2"]
            for etape_travaux_name in conso_etape_travaux:
                etape_travaux.find(f'{etape_travaux_name}').text = str(float(etape_travaux.find(f'{etape_travaux_name}').text) * 100)
            engine.controle_coherence_conso_etape_travaux(logement, report)
            assert (len(report.warning_software) == 1)

def test_controle_coherence_presence_derogation_ventilation():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.1_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_presence_derogation_ventilation(logement, report)
        assert (len(report.error_input) == 0)

    # Modifie chaque logement, pour qu'il y ait une incohérence entre le enum_derogation_ventilation_id et le enum_etat_ventilation_id
    enum_derogation_ventilation_id = audit.find('.//administratif').find('enum_derogation_ventilation_id')
    enum_derogation_ventilation_id.text = "1"  # "1": "abscence de dérogation"
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        enum_etat_ventilation_id = logement.find('*//enum_etat_ventilation_id')
        enum_etat_ventilation_id.text = "3"  # "3": "cas de dérogation"
        engine.controle_coherence_presence_derogation_ventilation(logement, report)
        assert (len(report.error_input) == 1)

def test_controle_coherence_abscence_derogation_ventilation():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.1_valid.xml'))
    # cas de base valide
    audit = etree.parse(f, parser)
    report = ReportAudit()
    engine.controle_coherence_abscence_derogation_ventilation(audit, report)
    assert (len(report.error_input) == 0)

    # Mettre chaque logement à "ventilation fonctionnelle" (pas de dérogation), et déclare une dérogation dans enum_derogation_ventilation_id
    enum_derogation_ventilation_id = audit.find('.//administratif').find('enum_derogation_ventilation_id')
    enum_derogation_ventilation_id.text = "3"  # "3": "ventilation naturelle efficace"
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        enum_etat_ventilation_id = logement.find('*//enum_etat_ventilation_id')
        enum_etat_ventilation_id.text = "2"  # "2": "ventilation fonctionnelle",

    engine.controle_coherence_abscence_derogation_ventilation(audit, report)
    assert (len(report.error_input) == 1)

def test_controle_coherence_ubat_base_ubat():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.1_valid.xml'))

    # Cas de base valide
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_ubat_base_ubat(logement, report)
        assert (len(report.warning_input) == 0)

    # Modifie chaque logement, pour qu'il y ait une incohérence entre ubat et ubat_base
    ubat_base = audit.find('*//ubat_base')
    if ubat_base is not None:
        ubat_base.text = "10"  # Set le ubat_base pour être sûr de sa valeur
        for logement in list(audit.iterfind('*//logement')):
            enum_scenario_id = logement.find('*//enum_scenario_id')
            enum_etape_id = logement.find('*//enum_etape_id')
            is_scenario_principal = enum_scenario_id.text in ["1", "2"]
            is_etape_finale = enum_etape_id.text == "2"  # "étape finale"
            report = ReportAudit()
            ubat = logement.find('*//ubat')
            ubat.text = "20"  # Modifie ubat pour qu'il soit supérieur à ubat_base
            engine.controle_coherence_ubat_base_ubat(logement, report)

            # Le controle ne doit s'appliquer que si is_scenario_principal ET is_etape_finale
            if is_scenario_principal and is_etape_finale:
                assert (len(report.warning_input) == 1)
            else:
                # le controle ne doit pas s'appliquer !
                assert (len(report.warning_input) == 0)


def test_controle_coherence_etat_ventilation():
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_audit_maison_1_v2.1_valid.xml'))

    # Cas de base valide pour les scénarios principaux
    audit = etree.parse(f, parser)
    for logement in list(audit.iterfind('*//logement')):
        report = ReportAudit()
        engine.controle_coherence_etat_ventilation(logement, report)
        assert (len(report.warning_input) == 0)

    # Cas de ventilation non fonctionnelle pour les scénarios principaux
    for logement in list(audit.iterfind('*//logement')):
        enum_etat_ventilation_id = logement.find('*//enum_etat_ventilation_id')
        enum_scenario_id = logement.find('*//enum_scenario_id')
        is_scenario_principal = enum_scenario_id.text in ["1", "2"]
        if is_scenario_principal:
            enum_etat_ventilation_id.text = "1"  # Modifie l'état de ventilation pour "ventilation non fonctionnelle"
            report = ReportAudit()
            engine.controle_coherence_etat_ventilation(logement, report)
            assert (len(report.warning_input) == 1)

    # Cas de ventilation non fonctionnelle pour les scénarios additionnels
    for logement in list(audit.iterfind('*//logement')):
        enum_etat_ventilation_id = logement.find('*//enum_etat_ventilation_id')
        enum_scenario_id = logement.find('*//enum_scenario_id')
        is_scenario_additional = enum_scenario_id.text in ["3", "4", "5"]
        if is_scenario_additional:
            enum_etat_ventilation_id.text = "1"  # Modifie l'état de ventilation pour "ventilation non fonctionnelle"
            report = ReportAudit()
            engine.controle_coherence_etat_ventilation(logement, report)
            assert (len(report.warning_input) == 1)

    # Cas de ventilation fonctionnelle pour tous les scénarios
    for logement in list(audit.iterfind('*//logement')):
        enum_etat_ventilation_id = logement.find('*//enum_etat_ventilation_id')
        enum_scenario_id = logement.find('*//enum_scenario_id')
        enum_etat_ventilation_id.text = "2"  # Modifie l'état de ventilation pour "ventilation fonctionnelle"
        report = ReportAudit()
        engine.controle_coherence_etat_ventilation(logement, report)
        assert (len(report.warning_input) == 0)



# ==================      TEST CONTROLE COHERENCE AUDIT ================

@pytest.mark.parametrize("valid_example", VALID_CASES_AUDIT)
def test_run_controle_coherence_audit(valid_example):
    try:
        _set_version_audit_to_valid_dates()
        engine = EngineAudit()
        parser = etree.XMLParser(remove_blank_text=True)

        # vérification v2
        f = str((engine.mdd_path / 'exemples_metier' / valid_example))
        audit = etree.parse(f, parser)
        report = engine.run_controle_coherence(audit)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

        # vérification rétrocompatibilité version précédente :
        # SAUF pour la 2.0 où la rétrocompatibilité n'est plus assuré avec la 1.1 (car la 1.1 n'est plus accepté en base ademe).
        get_previous_version = {'1.0':'1.0', '1.1':'1.0', '2.0':'2.0', '2.1':'2.0'}
        audit = etree.parse(f, parser)
        audit.find('*//enum_version_audit_id').text = get_previous_version[audit.find('*//enum_version_audit_id').text]
        report=engine.run_controle_coherence(audit)
        nb_errors = len(report['erreur_logiciel']) + len(report['erreur_saisie'])
        assert (report['validation_xsd']['valid'] == True)
        assert (nb_errors == 0)

    except AssertionError as e:
        print(report['validation_xsd'])
        print(f'nombre d erreurs : {nb_errors}')
        for err in report['erreur_logiciel'] + report['erreur_saisie']:
            print("=================== ERROR =========================")
            print(err['thematique'])
            print(err['message'])
            print(err['objets_concerne'])
        print(f)
        raise e

# verifier que des XML à trous ne fassent pas planter le moteur de controle de cohérence AUDIT
@pytest.mark.parametrize("valid_example", VALID_CASES_AUDIT)
def test_do_not_crash_controle_coherence_audit_(valid_example):
    # Load and parse the xml file
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f_xml = str((engine.mdd_path / 'exemples_metier' / valid_example))
    audit = etree.parse(f_xml, parser)
    xml_root = audit.getroot()

    enum_version_audit_id = audit.find('.//enum_version_audit_id').text
    xsd_file = versions_audit_cfg[enum_version_audit_id]['xsd_file']
    # Load and parse the xsd file

    f_xsd = str((engine.mdd_path / xsd_file))
    xsd_tree = etree.parse(f_xsd, parser)
    xsd_root = xsd_tree.getroot()

    # Find all elements in the xsd file that are optional
    optional_elements = xsd_root.xpath('.//xs:element[@minOccurs="0"][@nillable="true"]',
                                       namespaces={'xs': 'http://www.w3.org/2001/XMLSchema'})

    # Get the names of these elements
    optional_element_names = [element.get('name') for element in optional_elements]

    # Remove the optional elements from the xml file
    for element_name in optional_element_names:
        for element in xml_root.xpath(f'//{element_name}'):
            element.getparent().remove(element)

    # vérification : le moteur ne doit pas planter !
    report = engine.run_controle_coherence(audit)

# verifier que des XML à trous, avec etape_travaux, ne fassent pas planter le moteur de controle de cohérence AUDIT
@pytest.mark.parametrize("valid_example", VALID_CASES_AUDIT)
def test_do_not_crash_with_etape_travaux_controle_coherence_audit_(valid_example):
    # Load and parse the xml file
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f_xml = str((engine.mdd_path / 'exemples_metier' / valid_example))
    audit = etree.parse(f_xml, parser)
    xml_root = audit.getroot()

    enum_version_audit_id = audit.find('.//enum_version_audit_id').text
    xsd_file = versions_audit_cfg[enum_version_audit_id]['xsd_file']
    # Load and parse the xsd file

    f_xsd = str((engine.mdd_path / xsd_file))
    xsd_tree = etree.parse(f_xsd, parser)
    xsd_root = xsd_tree.getroot()

    # Find all elements in the xsd file that are optional
    optional_elements = xsd_root.xpath('.//xs:element[@minOccurs="0"][@nillable="true"]',
                                       namespaces={'xs': 'http://www.w3.org/2001/XMLSchema'})

    # Get the names of these elements
    optional_element_names = [element.get('name') for element in optional_elements]

    # Remove the optional elements from the xml file
    for element_name in optional_element_names:
        # Don't remove 'etape_travaux'
        if element_name == 'etape_travaux':
            continue
        for element in xml_root.xpath(f'//{element_name}'):
            element.getparent().remove(element)

    # vérification : le moteur ne doit pas planter !
    report = engine.run_controle_coherence(audit)


# verifier que des XML (mais qui sont valid pour le XSD) ne fassent pas planter le moteur de controle de cohérence AUDIT
@pytest.mark.parametrize("crash_example", CRASH_CASES_AUDIT)
def test_do_not_crash_files_controle_coherence_audit_(crash_example):
    # Load and parse the xml file
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f_xml = str((engine.mdd_path / 'exemples_metier' / crash_example))
    audit = etree.parse(f_xml, parser)

    # vérification : le moteur ne doit pas planter !
    report = engine.run_controle_coherence(audit)
    stop = 1

# verifier que des XML des EDL (mais qui sont valid pour le XSD) ne fassent pas planter le moteur de controle de cohérence AUDIT
@pytest.mark.parametrize("crash_example", CRASH_CASES_EDL_AUDIT)
def test_do_not_crash_edl_files_controle_coherence_audit_(crash_example):
    # Load and parse the xml file
    engine = EngineAudit()
    parser = etree.XMLParser(remove_blank_text=True)
    f_xml = str((engine.mdd_path / 'exemples_metier' / crash_example))
    audit = etree.parse(f_xml, parser)

    # vérification : le moteur ne doit pas planter !
    report = engine.run_controle_coherence(audit)


@pytest.mark.skipif(CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST == 0, reason="extensive test is very long")
def test_extensive_random_change_param():
    _set_version_dpe_to_valid_dates()

    import random
    random.seed(0)
    engine = EngineDPE()

    for a_file in ['cas_test_appt_2_neuf_valid.xml', 'cas_test_immeuble_1_valid_avec_reference.xml', "cas_test_tertiaire_1_vierge_valid.xml"]:
        print(a_file)
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        el_tv = dpe.xpath('*//*[starts-with(name(), "tv_")]')
        el_enum = dpe.xpath('*//*[starts-with(name(), "enum_")]')
        el_all = el_tv + el_enum
        for i in range(100):
            random.shuffle(el_all)
            for el in el_all:
                if el.tag.startswith('tv'):
                    el.text = str(random.choice(list(engine.valeur_table_dict[el.tag].keys())))
                else:
                    el.text = str(random.choice(list(engine.enum_dict[el.tag].keys())))
                report = ReportDPE()
                engine.run_controle_coherence(dpe)
    for a_file in ['cas_test_immeuble_1_valid.xml', 'cas_test_appt_2_neuf_valid.xml']:
        print(a_file)
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        el_tv = dpe.xpath('*//*[starts-with(name(), "tv_")]')
        el_enum = dpe.xpath('*//*[starts-with(name(), "enum_")]')
        el_all = el_tv + el_enum
        for i in range(100):
            random.shuffle(el_all)
            for el in el_all:
                if el.tag.startswith('tv'):
                    el.text = str(random.choice(list(engine.valeur_table_dict[el.tag].keys())))
                else:
                    el.text = str(random.choice(list(engine.enum_dict[el.tag].keys())))
                report = ReportDPE()
                engine.run_controle_coherence(dpe)


@pytest.mark.skipif(CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST == 0, reason="extensive test is very long")
def test_extensive_random_remove_optionnel_controle_coherence():
    _set_version_dpe_to_valid_dates()

    import random
    random.seed(0)
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    xs = '{http://www.w3.org/2001/XMLSchema}'
    namespaces = {'xs': 'http://www.w3.org/2001/XMLSchema'}
    f = str((engine.mdd_path / 'exemples_metier' / 'cas_test_appt_1.xml'))
    dpe = etree.parse(f, parser)
    engine.run_controle_coherence(dpe)
    for a_file in ['cas_test_appt_2_neuf_valid.xml', 'cas_test_immeuble_1_valid_avec_reference.xml', "cas_test_tertiaire_1_vierge_valid.xml"]:
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        version_id_str = dpe.find('*//enum_version_id').text

        # path_xsd = r'.\DPE_dev.xsd'
        et_xsd = etree.parse(str((engine.mdd_path / versions_dpe_cfg[version_id_str]['xsd_file']).absolute()), parser=parser)
        root = et_xsd.getroot()
        path_list = list()
        for el in [el for el in root.xpath('//xs:element[@minOccurs="0"]', namespaces=namespaces)]:
            if el.find('xs:annotation/xs:appinfo', namespaces=namespaces) is not None:
                source = el.find('xs:annotation/xs:appinfo', namespaces=namespaces).attrib.get('source')
                source = source[3:]
                path_list.append(source)
        for i in range(100):

            dpe = etree.parse(f, parser)
            all_optional_elements = list()
            for path in path_list:
                all_optional_elements.extend(list(dpe.iterfind(path)))
            random.shuffle(all_optional_elements)
            for el in all_optional_elements:
                el.getparent().remove(el)
                engine.run_controle_coherence(dpe)

    for a_file in ['cas_test_immeuble_1_valid.xml', 'cas_test_appt_2_neuf_valid.xml']:
        parser = etree.XMLParser(remove_blank_text=True)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        version_id_str = (dpe.find('*//enum_version_id').text)

        # path_xsd = r'.\DPE_dev.xsd'
        et_xsd = etree.parse(str((engine.mdd_path / versions_dpe_cfg[version_id_str]['xsd_file']).absolute()), parser=parser)
        root = et_xsd.getroot()
        path_list = list()
        for el in [el for el in root.xpath('//xs:element[@minOccurs="0"]', namespaces=namespaces)]:
            if el.find('xs:annotation/xs:appinfo', namespaces=namespaces) is not None:
                source = el.find('xs:annotation/xs:appinfo', namespaces=namespaces).attrib.get('source')
                source = source[3:]
                path_list.append(source)
        for i in range(100):

            dpe = etree.parse(f, parser)
            all_optional_elements = list()
            for path in path_list:
                all_optional_elements.extend(list(dpe.iterfind(path)))
            random.shuffle(all_optional_elements)
            for el in all_optional_elements:
                el.getparent().remove(el)
                engine.run_controle_coherence(dpe)


@pytest.mark.skipif(CONTROLE_COHERENCE_RUN_EXTENSIVE_TEST == 0, reason="extensive test is very long")
def test_extensive_random_chaos():
    import random
    engine = EngineDPE()
    parser = etree.XMLParser(remove_blank_text=True)
    namespaces = {'xs': 'http://www.w3.org/2001/XMLSchema'}

    for a_file in ['cas_test_appt_2_neuf_valid.xml', 'cas_test_immeuble_1_valid_avec_reference.xml', "cas_test_tertiaire_1_vierge_valid.xml"]:
        print(a_file)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        version_id_str = dpe.find('*//enum_version_id').text
        # path_xsd = r'.\DPE_dev.xsd'
        et_xsd = etree.parse(str((engine.mdd_path / versions_dpe_cfg[version_id_str]['xsd_file']).absolute()), parser=parser)
        root = et_xsd.getroot()
        el_tv = dpe.xpath('*//*[starts-with(name(), "tv_")]')
        el_enum = dpe.xpath('*//*[starts-with(name(), "enum_")]')
        el_all = el_tv + el_enum

        path_list = list()
        for el in [el for el in root.xpath('//xs:element[@minOccurs="0"]', namespaces=namespaces)]:
            if el.find('xs:annotation/xs:appinfo', namespaces=namespaces) is not None:
                source = el.find('xs:annotation/xs:appinfo', namespaces=namespaces).attrib.get('source')
                source = source[3:]
                path_list.append(source)

        for i in range(1000):
            dpe = etree.parse(f, parser)
            all_optional_elements = list()
            for path in path_list:
                all_optional_elements.extend(list(dpe.iterfind(path)))
            random.shuffle(all_optional_elements)
            random.shuffle(el_all)
            for i in range(0, 10):
                for el in el_all:
                    # randomly pop an optional element
                    if random.random() > 0.5:
                        if len(all_optional_elements) > 0:
                            el_opt = all_optional_elements.pop(0)
                            el_opt.getparent().remove(el_opt)
                    if el.tag.startswith('tv'):
                        el.text = str(random.choice(list(engine.valeur_table_dict[el.tag].keys())))
                    else:
                        el.text = str(random.choice(list(engine.enum_dict[el.tag].keys())))
                    report = ReportDPE()
                    engine.run_controle_coherence(dpe)

    for a_file in VALID_CASES_DPE:
        print(a_file)
        f = str((engine.mdd_path / 'exemples_metier' / a_file))
        dpe = etree.parse(f, parser)
        version_id_str = dpe.find('*//enum_version_id').text
        # path_xsd = r'.\DPE_dev.xsd'
        et_xsd = etree.parse(str((engine.mdd_path / versions_dpe_cfg[version_id_str]['xsd_file']).absolute()), parser=parser)
        root = et_xsd.getroot()
        el_tv = dpe.xpath('*//*[starts-with(name(), "tv_")]')
        el_enum = dpe.xpath('*//*[starts-with(name(), "enum_")]')
        el_all = el_tv + el_enum

        path_list = list()
        for el in [el for el in root.xpath('//xs:element[@minOccurs="0"]', namespaces=namespaces)]:
            if el.find('xs:annotation/xs:appinfo', namespaces=namespaces) is not None:
                source = el.find('xs:annotation/xs:appinfo', namespaces=namespaces).attrib.get('source')
                source = source[3:]
                path_list.append(source)

        for i in range(1000):
            dpe = etree.parse(f, parser)
            all_optional_elements = list()
            for path in path_list:
                all_optional_elements.extend(list(dpe.iterfind(path)))
            random.shuffle(all_optional_elements)
            random.shuffle(el_all)
            for i in range(0, 10):
                for el in el_all:
                    # randomly pop an optional element
                    if random.random() > 0.5:
                        if len(all_optional_elements) > 0:
                            el_opt = all_optional_elements.pop(0)
                            el_opt.getparent().remove(el_opt)
                    if el.tag.startswith('tv'):
                        el.text = str(random.choice(list(engine.valeur_table_dict[el.tag].keys())))
                    else:
                        el.text = str(random.choice(list(engine.enum_dict[el.tag].keys())))
                    report = ReportDPE()
                    engine.run_controle_coherence(dpe)
