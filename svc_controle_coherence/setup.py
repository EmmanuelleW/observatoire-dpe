from setuptools import setup, find_packages

setup(
    name='controle_coherence',
    packages=find_packages(),
    version='2.0.0',
    url='https://gitlab.com/observatoire-dpe/observatoire-dpe')
