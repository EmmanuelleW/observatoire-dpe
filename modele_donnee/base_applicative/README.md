ressources spécifiques à la base applicative de l'ADEME

exclusion_objet : liste des objets du xml à exclure de la base de données (car champ texte non structuré)

export_excel_base_applicative : excel enumérateur rangés de manière différente pour la base applicative. 